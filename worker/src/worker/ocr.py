import io
import logging

import PIL.Image
import PIL.ImageDraw
import pycountry
import pyocr
import pyocr.builders

import openpaper.backend.models.scannerdb  # noqa: E402


LOGGER = logging.getLogger(__name__)


class Ocr:
    def __init__(self):
        tools = pyocr.get_available_tools()
        for tool in tools:
            LOGGER.info(f"[OCR] Found OCR tool: {tool.get_name()}")
        self.tool = tools[0]
        LOGGER.info(f"[OCR] Will use: {self.tool.get_name()}")
        langs = self.tool.get_available_languages()
        self.langs = {}
        for lang in langs:
            short_lang = lang.split("_")[0]
            if short_lang in self.langs:
                self.langs[short_lang] += f"+{lang}"
            else:
                self.langs[short_lang] = lang
        LOGGER.info(f"[OCR] Available languages: {self.langs}")

    def _pycountry_to_tesseract(self, ocr_langs):
        attrs = [
            'iso639_3_code',
            'terminology',
            'alpha_3',
            'bibliographic',
        ]
        for attr in attrs:
            r = getattr(ocr_langs, attr, None)
            if r is not None and r in self.langs:
                return self.langs[r]
        return None

    def _find_language_for_ocr(self, lang_str, default="eng+deu+fra"):
        if lang_str is None:
            return default

        lang_str = lang_str.lower()
        if "_" in lang_str:
            lang_str = lang_str.split("_")[0]

        LOGGER.info(f"[OCR] Language to convert: {lang_str}")

        attrs = (
            'iso_639_3_code',
            'iso639_3_code',
            'iso639_2T_code',
            'iso639_1_code',
            'terminology',
            'bibliographic',
            'alpha_3',
            'alpha_2',
            'alpha2',
            'name',
        )
        for attr in attrs:
            try:
                r = pycountry.languages.get(**{attr: lang_str})
                if r is not None:
                    r = self._pycountry_to_tesseract(r)
                    if r is not None:
                        LOGGER.info(f"[OCR] OCR language: {r}")
                        return r
            except (KeyError, UnicodeDecodeError):
                pass

        LOGGER.info(
            "[OCR] [Warning]"
            f" Unknown language [{lang_str}]. Falling back to {default}",
        )
        return default

    def _detect_orientation(self, img, locale):
        LOGGER.info("[OCR] Detecting orientation ...")
        orientation = 0
        if self.tool.can_detect_orientation():
            try:
                orientation = self.tool.detect_orientation(
                    img, lang=locale
                )['angle']
            except pyocr.PyocrException as exc:
                LOGGER.info(
                    "[OCR] [WARNING] Orientation detection failed:"
                    f" {repr(exc)}"
                )
        LOGGER.info(f"[OCR] Orientation: {orientation}")
        return orientation

    def _write_hocr(self, boxes, report, file_base_name):
        LOGGER.info("[OCR] Generating hOCR file ...")
        fd = io.StringIO()
        pyocr.builders.LineBoxBuilder.write_file(fd, boxes)
        fd.seek(0)
        new_attachment = (
            openpaper.backend.models.scannerdb.ScanReportAttachment(
                report=report,
                file_name=f"{file_base_name}.hocr",
            )
        )
        new_attachment.attachment.save(new_attachment.file_name, fd)
        new_attachment.save()
        LOGGER.info("[OCR] hOCR file generated")

    def _draw_boxes(self, img, boxes, report, file_base_name):
        LOGGER.info("[OCR] Drawing boxes")
        if img.mode == "RGB":
            outline = (0, 0, 255)
        else:
            outline = 0
        draw = PIL.ImageDraw.Draw(img)
        for line in boxes:
            draw.rectangle(
                line.position,
                outline=outline
            )
            for word in line.word_boxes:
                draw.rectangle(
                    word.position,
                    outline=outline
                )
        fd = io.BytesIO()
        img.save(fd, "PNG")
        fd.seek(0)
        new_attachment = (
            openpaper.backend.models.scannerdb.ScanReportAttachment(
                report=report,
                file_name=f"{file_base_name}_boxes.png",
            )
        )
        new_attachment.attachment.save(new_attachment.file_name, fd)
        new_attachment.save()
        LOGGER.info("[OCR] Drawing done")

    def _do(self, attachment_id):
        LOGGER.info(f"[OCR] do({attachment_id})")

        attachment = (
            openpaper.backend.models.scannerdb
            .ScanReportAttachment.objects.get(
                id=attachment_id
            )
        )
        file_name = attachment.file_name
        locale = attachment.report.locale
        locale = self._find_language_for_ocr(locale)

        if "." not in file_name:
            LOGGER.info(f"[OCR] ERROR: file name {file_name} has not extension")
            return
        (file_base_name, file_ext) = file_name.rsplit(".", 1)

        LOGGER.info("[OCR] Running OCR ...")
        try:
            with PIL.Image.open(attachment.attachment.open("rb")) as img:
                if img.width * img.height >= 89478485:
                    LOGGER.info(
                        "[OCR] ERROR: Image too big:"
                        f" {img.width}x{img.height}"
                    )
                    return

                angle = self._detect_orientation(img, locale)
                if angle != 0:
                    angle = {
                        90: PIL.Image.ROTATE_90,
                        180: PIL.Image.ROTATE_180,
                        270: PIL.Image.ROTATE_270,
                    }[angle]
                    img = img.transpose(angle)

                LOGGER.info("[OCR] Running OCR ...")
                boxes = self.tool.image_to_string(
                    img,
                    lang=locale,
                    builder=pyocr.builders.LineBoxBuilder()
                )
                LOGGER.info("[OCR] OCR done")

                self._write_hocr(boxes, attachment.report, file_base_name)
                self._draw_boxes(
                    img, boxes, attachment.report, file_base_name
                )

            LOGGER.info("[OCR] All done !")
        except FileNotFoundError:
            LOGGER.info(
                "[OCR] [ERROR] File %s doesn't exist !",
                attachment.attachment.path
            )

    def do(self, attachment_id):
        self._do(attachment_id)
