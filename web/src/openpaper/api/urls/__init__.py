from django.urls import (
    include,
    path
)

from . import scannerdb


urlpatterns = [
    path('v1/scannerdb/', include(scannerdb.urlpatterns)),
]
