import collections

from django.db import migrations


def simplify_version(paperwork_version):
    paperwork_version = paperwork_version.split("-", 1)[0]
    if ".dev" in paperwork_version:
        paperwork_version = paperwork_version.split(".dev", 1)[0]
        paperwork_version = paperwork_version.split(".")
        paperwork_version[-1] = str(int(paperwork_version[-1]) - 1)
        paperwork_version = ".".join(paperwork_version)
    paperwork_version = paperwork_version[:30]
    return paperwork_version


def fix_stats_versions(app, schema_editor):
    Statistics = app.get_model('beacon', 'Statistics')
    for stat in Statistics.objects.all():
        stat.paperwork_version = simplify_version(stat.paperwork_version)
        stat.save()


def fix_stat_history_versions(app, schema_editor):
    StatsHistory = app.get_model('beacon', 'StatsHistory')
    StatsHistoryVersion = app.get_model('beacon', 'StatsHistoryVersion')

    for history in StatsHistory.objects.all():
        # recount
        hist_versions = StatsHistoryVersion.objects.filter(history=history)
        version_counts = collections.defaultdict(lambda: 0)
        for hist_version in hist_versions:
            version = simplify_version(hist_version.paperwork_version)
            version_counts[version] += hist_version.count
        # reinsert
        hist_versions.delete()
        for (version, count) in version_counts.items():
            new_hist_version = StatsHistoryVersion(
                history=history, paperwork_version=version, count=count
            )
            new_hist_version.save()


class Migration(migrations.Migration):
    dependencies = [
        ('beacon', '0007_auto_20201214_2058'),
    ]

    operations = [
        migrations.RunPython(fix_stats_versions),
        migrations.RunPython(fix_stat_history_versions),
    ]
