import django.contrib.admin

from ..models import scannerdb


class BaseAdmin(django.contrib.admin.ModelAdmin):
    def save_model(self, request, obj, form, change):
        super().save_model(request, obj, form, change)
        scannerdb.invalidate_cache()

    def delete_model(self, request, obj):
        super().delete_model(request, obj)
        scannerdb.invalidate_cache()


class ReportAdmin(BaseAdmin):
    list_display = (
        "creation_date",
        "moderated",
        "scanner",
        "user_email",
        "successful",
    )

    def formfield_for_foreignkey(self, db_field, request, **kwargs):
        if db_field.name == "scanner":
            kwargs["queryset"] = (
                scannerdb.Scanner.objects.order_by(
                    "manufacturer__name", "name"
                )
            )
        return super().formfield_for_foreignkey(db_field, request, **kwargs)


django.contrib.admin.site.register(scannerdb.Scanner, BaseAdmin)
django.contrib.admin.site.register(scannerdb.ScannerManufacturer, BaseAdmin)
django.contrib.admin.site.register(scannerdb.ScanReport, ReportAdmin)
django.contrib.admin.site.register(scannerdb.ScanReportAttachment, BaseAdmin)
django.contrib.admin.site.register(scannerdb.ScanReportFieldInteger, BaseAdmin)
django.contrib.admin.site.register(scannerdb.ScanReportFieldString, BaseAdmin)
django.contrib.admin.site.register(scannerdb.ScanReportTreeNode, BaseAdmin)
