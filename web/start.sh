#!/bin/sh

sleep 3

echo "Migrate"
if ! python3 ./manage.py migrate ; then
  exit 1
fi

if [ -n "${DJANGO_ADMIN_USER}" ] ; then
  echo "Creating super user ${DJANGO_ADMIN_USER}"
  python3 ./manage.py shell -c \
    "from django.contrib.auth.models import User; User.objects.create_superuser('${DJANGO_ADMIN_USER}', 'admin@example.com', '${DJANGO_ADMIN_PASSWD}')" \
    2>/dev/null >/dev/null
  if [ $? -ne 0 ]; then
    echo "Failed (user probably already exist)"
  else
    echo "Success"
  fi
fi

echo "Chown"
if ! chown -R www-data:www-data /var/www/uploads ; then
  exit 1
fi

echo "Clear cache"
python3 ./manage.py shell << EOF
import django.core.cache
django.core.cache.cache.clear()
EOF
r=$?
if [ $r -gt 0 ] ; then
  exit 1
fi

echo "Ready"
apachectl -D FOREGROUND
exit $?
