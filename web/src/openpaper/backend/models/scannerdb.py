import collections
import json
import logging
import os

import django.core.cache
import django.db.models
import django.db.transaction
import django.contrib.contenttypes.fields
import django.contrib.contenttypes.models
import django.http
import django.urls


from .. import util
from .. import msgqueue


LOGGER = logging.getLogger(__name__)


class ScannerManufacturer(django.db.models.Model):
    creation_date = django.db.models.DateTimeField(auto_now_add=True)
    last_mod_date = django.db.models.DateTimeField(auto_now=True)
    moderated = django.db.models.BooleanField(default=False)
    name = django.db.models.CharField(
        max_length=128, null=False, blank=False, db_index=True,
        unique=True
    )

    def __str__(self):
        return self.name

    @property
    def user_url(self):
        return django.urls.reverse("scannerdb_no_i18n:vendor", args=[self.id])


class Scanner(django.db.models.Model):
    class Meta:
        index_together = [("name", "manufacturer")]
        unique_together = [("manufacturer", "name")]

    creation_date = django.db.models.DateTimeField(auto_now_add=True)
    last_mod_date = django.db.models.DateTimeField(auto_now=True)
    moderated = django.db.models.BooleanField(default=False)
    name = django.db.models.CharField(
        max_length=128, null=False, blank=False, db_index=True
    )
    manufacturer = django.db.models.ForeignKey(
        ScannerManufacturer, null=False, blank=False, db_index=True,
        related_name='scanners', on_delete=django.db.models.CASCADE
    )

    def __str__(self):
        return "{} {}".format(self.manufacturer.name, self.name)

    @property
    def user_url(self):
        return django.urls.reverse("scannerdb_no_i18n:scanner", args=[self.id])


class ScanReport(django.db.models.Model):
    creation_date = django.db.models.DateTimeField(
        auto_now_add=True, db_index=True
    )
    last_update = django.db.models.DateTimeField(auto_now=True)
    secret_key = django.db.models.CharField(
        max_length=64, null=False, blank=True, db_index=False
    )
    sealed = django.db.models.BooleanField(null=False)

    scanner = django.db.models.ForeignKey(
        Scanner, null=False, blank=False, db_index=True,
        related_name='reports', on_delete=django.db.models.CASCADE
    )

    moderated = django.db.models.BooleanField(default=False, db_index=True)
    successful = django.db.models.BooleanField(default=False, db_index=True)
    admin_comment = django.db.models.CharField(
        max_length=255, null=True, blank=True, default=None
    )

    user_email = django.db.models.EmailField(null=True, blank=True)

    os = django.db.models.CharField(
        max_length=128, null=False, blank=False, db_index=True
    )
    locale = django.db.models.CharField(
        max_length=128, null=True, default=None, db_index=False
    )
    scan_source = django.db.models.CharField(
        max_length=128, null=False, blank=True, db_index=False
    )
    scan_library = django.db.models.CharField(
        max_length=128, null=False, blank=False, db_index=False,
    )
    scan_program = django.db.models.CharField(
        max_length=128, null=False, blank=False, db_index=False,
    )
    scanner_type = django.db.models.CharField(
        max_length=64, null=False, blank=False, db_index=False,
    )

    def __str__(self):
        return "{} {}".format(self.scanner, self.id)

    @property
    def user_url(self):
        return django.urls.reverse("scannerdb_no_i18n:report", args=[self.id])

    def get_tree(self, request):
        """
        Return the report node tree as nested dictionnaries (can be converted
        in JSON easily)
        """
        class Node(object):
            def __init__(s, db_node):
                s.db = db_node
                s.children = []

            def to_dict(s):
                if (s.db is not None and
                        s.db.lft == s.db.rgt and
                        s.db.content is not None):
                    return s.db.content.to_dict(request, s.db)
                return {
                    child.db.name: child.to_dict()
                    for child in s.children
                }

        db_nodes = ScanReportTreeNode.objects.filter(
            report=self.id
        ).order_by("lft", "-rgt")

        tree_root = Node(None)

        for db_node in db_nodes:
            parent = tree_root
            while True:
                for child in parent.children:
                    if (child.db.lft <= db_node.lft and
                            db_node.rgt <= child.db.rgt):
                        parent = child
                        break  # will examine the children of this node
                else:
                    break  # we have found our parent

            new_node = Node(db_node)
            parent.children.append(new_node)

        return tree_root.to_dict()

    @django.db.transaction.atomic()
    def set_tree(self, tree):
        class Node(object):
            def __init__(s, json_content):
                s.json_content = json_content
                if isinstance(json_content, dict):
                    s.children = {
                        k: Node(child)
                        for (k, child) in json_content.items()
                    }
                else:
                    s.children = {}
                s.lft = -1
                s.rgt = -1

            def compute_lft_rgt(s, lft=0):
                if s.rgt >= 0:
                    return s.rgt
                s.lft = lft
                s.rgt = lft
                for child in s.children.values():
                    lft = child.compute_lft_rgt(lft) + 1
                s.rgt = lft
                return s.rgt

            def _insert(s, key):
                content = None
                if isinstance(s.json_content, int):
                    content = ScanReportFieldInteger(value=s.json_content)
                elif not isinstance(s.json_content, dict):
                    content = ScanReportFieldString(
                        value=str(s.json_content)
                    )
                if content is not None:
                    content.save()
                assert (s.lft >= 0)
                assert (s.rgt >= 0)
                ScanReportTreeNode.objects.create(
                    report=self,
                    name=key,
                    lft=s.lft,
                    rgt=s.rgt,
                    content=content
                )

            def insert(s):
                if not isinstance(s.json_content, dict):
                    return
                for (k, v) in s.children.items():
                    v.insert()
                    v._insert(k)

        root = Node(tree)
        root.compute_lft_rgt()
        root.insert()

    def get_node(self, node_path):
        # we look for the possible end nodes first and then check if their
        # path match
        leaf_nodes = ScanReportTreeNode.objects.filter(
            report=self, name=node_path[-1]
        )
        for leaf_node in leaf_nodes:
            path = leaf_node.get_path()
            path = [n.name for n in path]
            if path == node_path:
                return leaf_node

        raise django.http.Http404("Node node found in report {}: {}".format(
            self.id, node_path
        ))

    def get_node_value(self, node_path):
        node = self.get_node(node_path)
        return node.content


class ScanReportTreeNode(django.db.models.Model):
    """
    Represents a tree hierachy

    - Node 'root' (lft: 1 - rgt: 20)
      - Node 'tutu' (left: 1 - rgt: 10)
        - Node 'a' (id: 1, left: 1, rgt: 1)
        - Node 'b' (id: 2, left: 2, rgt: 2)
        - ...
      - Section 'titi' (left: 11 - rgt: 20)
        - Node 'a' (id: 11, left: 11, rgt: 11)
        - Node 'b' (id: 12, left: 12, rgt: 12)
        - ...

    See http://mikehillyer.com/articles/managing-hierarchical-data-in-mysql/
    """
    report = django.db.models.ForeignKey(
        ScanReport, null=False, blank=False, db_index=True,
        on_delete=django.db.models.CASCADE
    )
    name = django.db.models.CharField(
        max_length=128, null=False, blank=False, db_index=True
    )
    lft = django.db.models.IntegerField()
    rgt = django.db.models.IntegerField()

    content_type = django.db.models.ForeignKey(
        django.contrib.contenttypes.models.ContentType,
        null=True, default=None, blank=False, db_index=False,
        on_delete=django.db.models.CASCADE
    )
    content_id = django.db.models.PositiveIntegerField(null=True, default=None)
    content = django.contrib.contenttypes.fields.GenericForeignKey(
        'content_type', 'content_id'
    )

    def get_path(self):
        path = ScanReportTreeNode.objects.filter(
            report=self.report, lft__lte=self.lft, rgt__gte=self.rgt
        ).order_by("lft", "-rgt")
        return list(path)

    def __str__(self):
        return "{} ({} - {})".format(self.name, self.lft, self.rgt)


class ScanReportFieldString(django.db.models.Model):
    node = django.contrib.contenttypes.fields.GenericRelation(
        ScanReportTreeNode,
        content_type_field='content_type',
        object_id_field='content_id',
    )
    value = django.db.models.CharField(max_length=8190, db_index=True)

    def __str__(self):
        return "{} = {}".format(self.id, str(self.value))

    def to_dict(self, request, node):
        return self.value

    def get_http_response(self):
        return django.http.JsonResponse({"value": self.value})


class ScanReportFieldInteger(django.db.models.Model):
    node = django.contrib.contenttypes.fields.GenericRelation(
        ScanReportTreeNode,
        content_type_field='content_type',
        object_id_field='content_id',
    )
    value = django.db.models.BigIntegerField(db_index=True)

    def __str__(self):
        return "{} = {}".format(self.id, str(self.value))

    def to_dict(self, request, node):
        return self.value

    def get_http_response(self):
        return django.http.JsonResponse({"value": self.value})


class ScanReportAttachment(django.db.models.Model):
    report = django.db.models.ForeignKey(
        ScanReport, null=False, blank=False, db_index=True,
        related_name='attachments', on_delete=django.db.models.CASCADE
    )
    file_name = django.db.models.CharField(
        max_length=64, null=False, blank=False, db_index=False
    )
    attachment = django.db.models.FileField(
        upload_to='scan_report_attachments'
    )

    @property
    def mime_type(self):
        return util.guess_mime(self.file_name)

    @property
    def is_image(self):
        return self.mime_type.startswith("image/")

    @property
    def download_url(self):
        return django.urls.reverse(
            "scan_report_attachment_download", args=[self.id, self.file_name]
        )

    def unlink(self):
        LOGGER.info("Deleting %s", self.attachment.path)
        if os.path.exists(self.attachment.path):
            os.remove(self.attachment.path)
        self.delete()

    def is_analysis_result(self):
        return (
            self.file_name.lower().endswith("_boxes.png")
            or self.file_name.lower().endswith(".hocr")
        )

    def analyze(self):
        mime = self.mime_type
        if mime.startswith("image/"):
            LOGGER.info(
                "Requesting OCR on attachment: %s %s",
                self.file_name, mime
            )
            msgqueue.Task('ocr', {
                'attachment_id': self.id,
            }).send()

    def save(self, *args, **kwargs):
        super().save(*args, **kwargs)
        if self.is_analysis_result():
            return
        self.analyze()


def invalidate_cache():
    for k in [
                "os",
                "success_rate",
                "last_report_successful",
            ]:
        django.core.cache.cache.delete(k)


def get_operating_systems():
    cache = django.core.cache.cache.get("os")
    if cache is not None:
        return json.loads(cache)
    oses = ScanReport.objects.filter(moderated=True)
    oses = oses.values('os').distinct()
    oses = [os['os'].title() for os in oses]
    oses.sort()
    django.core.cache.cache.set("os", json.dumps(oses))
    return oses


def is_last_report_successful(oses, base_req, cache_key):
    cache = django.core.cache.cache.get("last_report_successful")
    if cache is not None:
        cache = json.loads(cache)
        if cache_key in cache:
            return cache[cache_key]

    out = []
    for operating_system in oses:
        report = base_req.filter(os__iexact=operating_system)
        report = report.order_by("-creation_date")[:1]
        if len(report) <= 0:
            out.append({'os': operating_system, 'success': None})
            continue
        out.append({'os': operating_system, 'success': report[0].successful})

    if cache is None:
        cache = {}
    cache[cache_key] = out
    django.core.cache.cache.set("last_report_successful", json.dumps(cache))

    return out


def get_success_rate(oses, base_req, cache_key):
    """
    Compute the success rate for each scanner manufacturer.
    Problem is that we must only take the latest moderated report into account
    each time.
    """
    cache = django.core.cache.cache.get("success_rate")
    if cache is not None:
        cache = json.loads(cache)
        if cache_key in cache:
            return cache[cache_key]

    totals = collections.defaultdict(
        lambda: collections.defaultdict(lambda: 0)
    )
    scanners = base_req.values("scanner__id").distinct()
    for scanner in scanners:
        successes = base_req.filter(scanner=scanner['scanner__id'])
        successes = is_last_report_successful(
            oses, successes,
            cache_key=f"scanner_{scanner['scanner__id']}"
        )
        for success in successes:
            if success['success'] is None:
                continue
            if success['success']:
                totals[success['os']]['success'] += 1
            else:
                totals[success['os']]['failed'] += 1

    stats = []
    for operating_system in oses:
        s = totals[operating_system]['success']
        f = totals[operating_system]['failed']
        if s + f == 0:
            stats.append({'os': operating_system, 'percent': None})
            continue
        stats.append({
            'os': operating_system,
            'percent': int((s / (s + f)) * 100),
        })

    if cache is None:
        cache = {}
    cache[cache_key] = stats
    django.core.cache.cache.set("success_rate", json.dumps(cache))

    return stats
