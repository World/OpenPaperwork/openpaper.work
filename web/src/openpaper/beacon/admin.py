from django.contrib import admin

from .models import (
    BugReport,
    BugReportAttachment,
    Statistics,
    StatsHistory,
    StatsHistoryVersion,
    StatsHistoryOs
)


class StatisticsAdmin(admin.ModelAdmin):
    readonly_fields = ('creation_date', 'last_mod_date',)
    list_display = (
        'uuid',
        'creation_date',
        'last_mod_date',
        'paperwork_version',
        'nb_documents',
        'os_name',
        'platform_processor',
        'platform_distribution',
        'country',
    )


admin.site.register(BugReport)
admin.site.register(BugReportAttachment)
admin.site.register(StatsHistory)
admin.site.register(StatsHistoryOs)
admin.site.register(StatsHistoryVersion)
admin.site.register(Statistics, StatisticsAdmin)
