from django.core.management.base import BaseCommand

import openpaper.backend.msgqueue


class Command(BaseCommand):
    help = "Trigger a recomputation of all bug clusters"

    def add_arguments(self, parser):
        pass

    def handle(self, *args, **options):
        print("Requesting recomputation of all bug clusters ...")
        openpaper.backend.msgqueue.Task('recompute_bug_clusters', {}).send()
        print("Request sent")
