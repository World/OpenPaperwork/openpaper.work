# -*- coding: utf-8 -*-
# Generated by Django 1.10.5 on 2017-04-29 12:14
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('beacon', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='statistics',
            name='uuid',
            field=models.BigIntegerField(db_index=True, unique=True),
        ),
    ]
