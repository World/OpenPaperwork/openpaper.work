from django.http import HttpResponseRedirect
from django.shortcuts import render
from django.views.decorators.cache import cache_page

import openpaper.beacon.views


@cache_page(10 * 60)
def index(request):
    return render(request, 'home.html', {})


@cache_page(10 * 60)
def download_linux(request):
    ctx = {
        'latest_paperwork_version': openpaper.beacon.views.LATEST_PAPERWORK_VERSION
    }
    return render(request, 'download_linux.html', ctx)


@cache_page(10 * 60)
def opensource(request):
    return render(request, 'opensource.html', {})


@cache_page(10 * 60)
def projects(request):
    return render(request, 'projects.html', {})


@cache_page(10 * 60)
def about(request):
    return render(request, 'about.html', {})


def redirect_to_index(request):
    return HttpResponseRedirect('/')
