from django import template
from django.urls import reverse


register = template.Library()


VALID_SETTINGS = {
    'filters',
    'min_docs',
    'split',
    'unit',
}


@register.simple_tag
def stats_url(settings):
    if 'filters' in settings:
        settings['filters'] = str(settings['filters'])
    if 'min_docs' in settings:
        settings['min_docs'] = str(settings['min_docs'])
    if 'split' in settings:
        if settings['split'] is None:
            settings['split'] = ''
    t = settings.pop('type')
    settings = {k: v for (k, v) in settings.items() if k in VALID_SETTINGS}
    return reverse('beacon:statistics_' + t, kwargs=settings)


@register.simple_tag
def stats_url_switch(settings, k, v):
    settings = settings.copy()
    settings[k] = v
    return stats_url(settings)


@register.simple_tag
def stats_url_filter_split(settings, filters, split):
    settings = settings.copy()
    settings['filters'] = filters
    settings['split'] = split
    return stats_url(settings)
