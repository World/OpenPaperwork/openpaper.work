import django.core.mail
import django.http
import django.shortcuts
import django.views

import rest_framework
import rest_framework.response
import rest_framework.viewsets

import openpaper.backend.models.scannerdb
from ..serializers import scannerdb as serializers_scannerdb
from ..permissions import scannerdb as permissions_scannerdb


class ScannerManufacturerViewSet(rest_framework.viewsets.ModelViewSet):
    queryset = openpaper.backend.models.scannerdb.ScannerManufacturer.objects.all(
    ).order_by("name")
    serializer_class = (
        serializers_scannerdb.ScannerManufacturerSerializer
    )
    permission_classes = [permissions_scannerdb.ScannerDbHasKeyOrReadOnly]


class ScannerViewSet(rest_framework.viewsets.ModelViewSet):
    queryset = openpaper.backend.models.scannerdb.Scanner.objects.all().order_by(
        "manufacturer__name", "name"
    )
    serializer_class = serializers_scannerdb.ScannerSerializer
    permission_classes = [permissions_scannerdb.ScannerDbHasKeyOrReadOnly]


class ScanReportViewSet(rest_framework.viewsets.ModelViewSet):
    permission_classes = [permissions_scannerdb.ScannerDbReportPermission]
    # default serializer ; we override it only in retrieve()
    serializer_class = serializers_scannerdb.DefaultScanReportSerializer

    def get_queryset(self):
        queryset = openpaper.backend.models.scannerdb.ScanReport.objects.order_by(
            "-creation_date"
        )
        if self.request.method not in rest_framework.permissions.SAFE_METHODS:
            return queryset
        return queryset.filter(moderated=True)

    def create(self, request):
        # custom create view that report the secret key.
        # this is the only time ever we want to report it.
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        self.perform_create(serializer)
        headers = self.get_success_headers(serializer.data)

        # a bit hackish, but does the trick
        data = serializer.data
        data['secret_key'] = serializer.instance.secret_key

        url = request.build_absolute_uri(
            serializer.instance.user_url
        )
        django.core.mail.mail_admins(
            "New scan report {}".format(serializer.instance.id),
            "New scan report available:\n" + url,
            fail_silently=True
        )

        return rest_framework.response.Response(
            data, status=rest_framework.status.HTTP_201_CREATED,
            headers=headers
        )

    def retrieve(self, request, pk=None):
        report = django.shortcuts.get_object_or_404(self.get_queryset(), pk=pk)
        serializer = (
            serializers_scannerdb.DetailedScanReportSerializer(
                report, context={'request': request}
            )
        )
        return rest_framework.response.Response(serializer.data)


class ScanReportAttachmentViewSet(rest_framework.viewsets.ModelViewSet):
    queryset = openpaper.backend.models.scannerdb.ScanReportAttachment.objects.filter()
    permission_classes = [
        permissions_scannerdb.ScannerDbReportAttachmentPermission
    ]
    serializer_class = serializers_scannerdb.ScanReportAttachmentSerializer


class ScanReportDataView(rest_framework.views.APIView):
    permission_classes = [permissions_scannerdb.ScannerDbReportPermission]

    def get_report(self, request, report_id, write=True):
        self.check_permissions(request)
        report = django.shortcuts.get_object_or_404(
            openpaper.backend.models.scannerdb.ScanReport.objects, pk=report_id
        )
        if write:
            self.check_object_permissions(request, report)
        return report

    def get(self, request, report_id, node_path):
        report = self.get_report(request, report_id, write=False)
        tree = report.get_tree(request)
        return django.http.response.JsonResponse(tree)

    def post(self, request, report_id):
        report = self.get_report(request, report_id, write=True)
        data = request.data['data']
        report.set_tree(data)
        return django.http.response.HttpResponse(status=201, reason="Created")


class ScanReportAttachmentDownload(django.views.View):
    def get(self, request, attachment_id, file_name):
        attachment = django.shortcuts.get_object_or_404(
            openpaper.backend.models.scannerdb.ScanReportAttachment.objects,
            pk=attachment_id
        )
        if attachment.file_name != file_name:
            raise django.http.Http404("Attachment not found")
        response = django.http.response.HttpResponse(
            attachment.attachment, content_type=attachment.mime_type
        )
        # response['Content-Disposition'] = 'attachment; filename={}'.format(
        #     attachment.file_name
        # )
        return response
