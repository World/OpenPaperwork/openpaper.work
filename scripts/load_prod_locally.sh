#!/usr/bin/env bash

SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )
cd ${SCRIPT_DIR}

if [ $UID -ne 0 ] ; then
	echo 'run with `sudo -E scripts/load_prod_locally.sh`'
	exit 1
fi

HOST=openpaper-docker-box.home

echo "# Getting prod container ID"
prod_container_id=$(ssh ${HOST} docker ps | grep openpaper_work_worker | cut -d' ' -f1)
if [ $? -ne 0 ] ; then exit $? ; fi
echo "# Prod container ID: ${prod_container_id}"

echo "# Getting prod DB password"
prod_password=$(ssh ${HOST} sudo grep MYSQL_PASSWORD= /etc/openpaper.work/db.env| cut -d= -f2)
if [ $? -ne 0 ] ; then exit $? ; fi

echo "# Getting local DB password"
local_password=$(grep MYSQL_PASSWORD= ../db.env | cut -d= -f2)
if [ $? -ne 0 ] ; then exit $? ; fi

echo "# Installing mariadbclient on prod container"
ssh ${HOST} docker exec ${prod_container_id} apt-get install -yqq mariadb-client
if [ $? -ne 0 ] ; then exit $? ; fi

echo "# Shutting down local Docker containers"
docker-compose down --volumes
if [ $? -ne 0 ] ; then exit $? ; fi

echo "# Copying uploads from prod to local"
ssh ${HOST} sudo tar -cf /dev/stdout /var/lib/docker/volumes/openpaperwork_openpaper-uploads \
	| tar -xC /
if [ $? -ne 0 ] ; then exit $? ; fi

echo "# Waiting for local DB"
while ! docker-compose run web nc -z db 3306 ;
do
	echo Waiting
	sleep 1
done

echo "# Restoring prod DB in local DB"
ssh ${HOST} docker exec -i ${prod_container_id} mysqldump -h db -u openpaper -p${prod_password} openpaper | \
	docker-compose run -T web mysql -h db -u openpaper -p${local_password} openpaper
if [ $? -ne 0 ] ; then exit $? ; fi

echo "# Cleanup"
rm -f ${tmp_sql}

echo "# Restarting local containers"
docker-compose up -d --build
if [ $? -ne 0 ] ; then exit $? ; fi

echo "# All done !"
