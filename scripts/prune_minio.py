#!/usr/bin/env python3

import pathlib
import os
import re
import tempfile

import git
import minio


BASE_GITLAB_URL = "https://gitlab.gnome.org/World/OpenPaperwork/"
PAPERWORK_REPO = BASE_GITLAB_URL + "paperwork.git"
IRONSCANNER_REPO = BASE_GITLAB_URL + "ironscanner.git"

DIRECTORIES = [
    'data/',
    'linux/amd64/',
    'linux/x86/',
    'windows/amd64/',
    'windows/x86/',
]

RE_COMMIT = re.compile(r"[a-fA-F0-9]{8}")


def get_minio_config():
    return {
        'host': os.environ['MINIO_HOST'],
        'access_key': os.environ['MINIO_ACCESS_KEY'],
        'secret_key': os.environ['MINIO_SECRET_KEY'],
        'bucket': os.environ['MINIO_BUCKET'],
    }


def get_tag_revisions(repo, tmp_dir):
    print(f"# Cloning {repo} ...")
    repo_dir = pathlib.Path(tmp_dir) / "repo.git"
    repo = git.Repo.clone_from(repo, repo_dir)
    print("# Getting tag revisions ...")
    tags = repo.tags
    for tag in tags:
        print(f"{tag.name} --> {tag.commit}")
    return {str(tag.commit)[:8].lower(): tag.name for tag in tags}


def main():
    minio_cfg = get_minio_config()

    with tempfile.TemporaryDirectory(prefix="prune_minio_") as tmp_dir:
        tag_revs = get_tag_revisions(PAPERWORK_REPO, tmp_dir)
    with tempfile.TemporaryDirectory(prefix="prune_minio_") as tmp_dir:
        tag_revs.update(get_tag_revisions(IRONSCANNER_REPO, tmp_dir))

    minio_client = minio.Minio(
        minio_cfg['host'],
        access_key=minio_cfg['access_key'],
        secret_key=minio_cfg['secret_key'],
    )

    to_delete = []
    for bucket_dir in DIRECTORIES:
        print(
            "# Listing objects in Minio bucket"
            f" {minio_cfg['bucket']}/{bucket_dir} ..."
        )
        objs = minio_client.list_objects(
            minio_cfg['bucket'],
            prefix=bucket_dir,
            recursive=True
        )
        for obj in objs:
            if "latest" in obj.object_name:
                print(
                    f"{obj.object_name} has 'latest' in its name. Won't delete"
                )
                continue
            for commit in RE_COMMIT.findall(obj.object_name):
                tag = tag_revs.get(commit.lower())
                if tag is not None:
                    print(
                        f"{obj.object_name} matches"
                        f" tag {commit.lower()} - {tag}. Won't delete"
                    )
                    break
            else:
                to_delete.append(obj.object_name)

    if len(to_delete) <= 0:
        print("# Nothing to delete")
        return

    print(f"Will delete {len(to_delete)} objects.")
    print("#####################################")
    for obj in to_delete:
        print(obj)
    print("#####################################")
    print("If it looks OK, press Enter. Otherwise Ctrl-C")
    input()

    to_delete = [minio.deleteobjects.DeleteObject(obj) for obj in to_delete]
    errors = minio_client.remove_objects(minio_cfg["bucket"], to_delete)
    nb_errors = 0
    for error in errors:
        print(f"# Error occurred when deleting object: {error}")
        nb_errors += 1
    if nb_errors <= 0:
        print("# All Done !")


if __name__ == "__main__":
    main()
