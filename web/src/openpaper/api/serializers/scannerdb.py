import django.http

import rest_framework
import rest_framework.serializers

import openpaper.backend.models.scannerdb

from . import util


class AbstractScannerDbSerializer(
        rest_framework.serializers.HyperlinkedModelSerializer):
    def to_representation(self, instance):
        r = super().to_representation(instance)
        if hasattr(instance, 'user_url'):
            r['user_url'] = self.context['request'].build_absolute_uri(
                instance.user_url
            )
        return r


class ScannerManufacturerSerializer(AbstractScannerDbSerializer):
    scanners = rest_framework.serializers.HyperlinkedRelatedField(
        many=True, read_only=True, view_name="scanner-detail"
    )

    class Meta:
        model = openpaper.backend.models.scannerdb.ScannerManufacturer
        fields = [
            "id",
            "creation_date",
            "last_mod_date",
            "moderated",
            "name",
            "scanners",
            "url",
        ]


class ScannerSerializer(AbstractScannerDbSerializer):
    reports = rest_framework.serializers.HyperlinkedRelatedField(
        many=True, read_only=True, view_name="scanreport-detail"
    )

    class Meta:
        model = openpaper.backend.models.scannerdb.Scanner
        fields = [
            "id",
            "creation_date",
            "last_mod_date",
            "manufacturer",
            "moderated",
            "name",
            "reports",
            "url",
        ]


class AbstractScanReportSerializer(AbstractScannerDbSerializer):
    def to_representation(self, instance):
        r = super().to_representation(instance)
        r.pop('user_email', None)
        return r


class DefaultScanReportSerializer(AbstractScanReportSerializer):
    class Meta:
        model = openpaper.backend.models.scannerdb.ScanReport
        fields = [
            "id",
            "admin_comment",
            "creation_date",
            "last_update",
            "locale",
            "moderated",
            "os",
            "scan_library",
            "scan_program",
            "scan_source",
            "scanner",
            "scanner_type",
            "sealed",
            "successful",
            "url",
            "user_email",
        ]

    def create(self, validated_data):
        # We want to generate the secret key only when creating a report.
        validated_data['secret_key'] = util.generate_secret(32)
        return super().create(validated_data)


class DetailedScanReportSerializer(AbstractScanReportSerializer):
    attachments = rest_framework.serializers.HyperlinkedRelatedField(
        many=True, read_only=True, view_name="scanreportattachment-detail"
    )

    class Meta:
        model = openpaper.backend.models.scannerdb.ScanReport
        fields = [
            "admin_comment",
            "attachments",
            "creation_date",
            "id",
            "last_update",
            "locale",
            "moderated",
            "os",
            "scan_library",
            "scan_program",
            "scan_source",
            "scanner",
            "scanner_type",
            "sealed",
            "successful",
            "url",
            "user_email",
        ]

    def to_representation(self, instance):
        r = super().to_representation(instance)
        r['data'] = instance.get_tree(self.context['request'])
        return r


class ScanReportAttachmentSerializer(AbstractScannerDbSerializer):
    class Meta:
        model = openpaper.backend.models.scannerdb.ScanReportAttachment
        fields = [
            "id",
            'report',
            'file_name',
            'attachment',
            'mime_type',
            'url',
        ]

    def create(self, attrs):
        instance = super().create(attrs)
        if instance.report.moderated or instance.report.sealed:
            raise django.http.Http404("Report is sealed")
        return instance

    def update(self, *args, **kwargs):
        raise Exception("Not supported")

    def to_representation(self, instance):
        r = super().to_representation(instance)
        r['attachment'] = self.context['request'].build_absolute_uri(
            instance.download_url
        )
        return r
