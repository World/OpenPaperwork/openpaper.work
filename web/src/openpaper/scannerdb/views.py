import logging

import django.db.models
import django.db.models.functions
import django.http
import django.views.decorators.http

import openpaper.backend.models.scannerdb
from . import forms


LOGGER = logging.getLogger(__name__)


DEFAULT_IMG = "unknown.png"

SCANNER_TYPES = {
    None: DEFAULT_IMG,
    # about 231x171
    "adf": "adf.png",
    "adf_duplex": "adf.png",
    "flatbed": "flatbed.png",
    "flatbed_adf": "flatbed_and_feeder.png",
    "flatbed_adf_duplex": "flatbed_and_feeder.png",
    "handheld": "handheld.png",
    "portable": "portable.png",
}


DEFAULT_CTX = {
    "oses": [],
    "selected_os": None,
    "manufacturers": [],
    "selected_manufacturer": None,
    "scanners": [],
    "selected_scanner": None,
    "reports": [],
    "selected_report": None,
}


@django.views.decorators.http.require_GET
def home(request):
    ctx = dict(DEFAULT_CTX)
    manufacturers = openpaper.backend.models.scannerdb.ScannerManufacturer.objects
    manufacturers = manufacturers.filter(moderated=True)
    manufacturers = manufacturers.order_by(
        django.db.models.functions.Lower('name')
    )
    ctx['manufacturers'] = manufacturers

    ctx['unmoderated'] = []
    if request.user.is_authenticated:
        ctx['unmoderated'] = (
            openpaper.backend.models.scannerdb.ScanReport.objects.filter(
                moderated=False,
                admin_comment=None,
            ).order_by('-creation_date')
        )

    stats = {}
    stats['oses'] = openpaper.backend.models.scannerdb.get_operating_systems()

    # Compute global stats
    stats['global'] = openpaper.backend.models.scannerdb.get_success_rate(
        stats['oses'],
        openpaper.backend.models.scannerdb.ScanReport.objects.filter(
            moderated=True,
            scanner__moderated=True,
            scanner__manufacturer__moderated=True,
        ),
        cache_key='global',
    )

    # Computer per manufacturer stats
    stats['per_manufacturer'] = []
    for manufacturer in manufacturers:
        stats['per_manufacturer'].append({
            "manufacturer": manufacturer,
            "stats": openpaper.backend.models.scannerdb.get_success_rate(
                stats['oses'],
                openpaper.backend.models.scannerdb.ScanReport.objects.filter(
                    moderated=True,
                    scanner__moderated=True,
                    scanner__manufacturer=manufacturer,
                ),
                cache_key=f"manufacturer_{manufacturer.id}",
            )
        })

    ctx['stats'] = stats
    return django.shortcuts.render(request, 'scandb_home.html', ctx)


@django.views.decorators.http.require_GET
def manufacturer(request, manufacturer_id):
    ctx = dict(DEFAULT_CTX)
    try:
        manufacturer = (
            openpaper.backend.models.scannerdb.ScannerManufacturer.objects.get(
                id=int(manufacturer_id)
            )
        )
        ctx['selected_manufacturer'] = manufacturer
    except (
                openpaper.backend.models.scannerdb.ScannerManufacturer.DoesNotExist,
                ValueError
            ):
        raise django.http.Http404(
            'Manufacturer {} not found'.format(manufacturer_id)
        )

    ctx['manufacturers'] = (
        openpaper.backend.models.scannerdb.ScannerManufacturer.objects.filter(
            moderated=True
        ).order_by("name")
    )

    scanners = openpaper.backend.models.scannerdb.Scanner.objects
    scanners = scanners.filter(
        moderated=True, manufacturer=ctx['selected_manufacturer']
    )
    scanners = scanners.order_by(django.db.models.functions.Lower("name"))
    ctx['scanners'] = scanners

    stats = {}
    stats['oses'] = openpaper.backend.models.scannerdb.get_operating_systems()
    stats['global'] = openpaper.backend.models.scannerdb.get_success_rate(
        stats['oses'],
        openpaper.backend.models.scannerdb.ScanReport.objects.filter(
            scanner__manufacturer=ctx['selected_manufacturer'],
            moderated=True,
            scanner__moderated=True,
            scanner__manufacturer__moderated=True,
        ),
        cache_key="global",
    )

    stats['per_scanner'] = []
    for scanner in scanners:
        stats['per_scanner'].append({
            'scanner': scanner,
            'successes': openpaper.backend.models.scannerdb.is_last_report_successful(
                stats['oses'],
                openpaper.backend.models.scannerdb.ScanReport.objects.filter(
                    scanner=scanner,
                    moderated=True,
                    scanner__moderated=True,
                    scanner__manufacturer__moderated=True,
                ),
                cache_key=(
                    f"scanner_{scanner.id}"
                ),
            ),
        })

    ctx['stats'] = stats
    return django.shortcuts.render(request, 'scandb_vendor.html', ctx)


def get_scanner_img(scanner):
    try:
        last_report = openpaper.backend.models.scannerdb.ScanReport.objects.filter(
            scanner=scanner
        ).order_by("-creation_date")[0]
        return SCANNER_TYPES.get(last_report.scanner_type, DEFAULT_IMG)
    except Exception:
        return DEFAULT_IMG


@django.views.decorators.http.require_GET
def scanner(request, scanner_id):
    ctx = dict(DEFAULT_CTX)
    try:
        ctx['selected_scanner'] = openpaper.backend.models.scannerdb.Scanner.objects.get(
            id=int(scanner_id)
        )
    except (openpaper.backend.models.scannerdb.Scanner.DoesNotExist, ValueError):
        raise django.http.Http404('Scanner {} not found'.format(scanner_id))
    ctx['selected_manufacturer'] = ctx['selected_scanner'].manufacturer
    ctx['manufacturers'] = (
        openpaper.backend.models.scannerdb.ScannerManufacturer.objects.filter(
            moderated=True
        ).order_by('name')
    )
    ctx['scanners'] = openpaper.backend.models.scannerdb.Scanner.objects.filter(
        moderated=True, manufacturer=ctx['selected_manufacturer']
    ).order_by("name")

    ctx['reports'] = openpaper.backend.models.scannerdb.ScanReport.objects.filter(
        moderated=True, scanner=ctx['selected_scanner']
    ).order_by("-creation_date")
    ctx['scanner_type_img'] = get_scanner_img(ctx['selected_scanner'])

    return django.shortcuts.render(request, 'scandb_scanner.html', ctx)


def get_report(report_id):
    try:
        report = openpaper.backend.models.scannerdb.ScanReport.objects.get(
            id=int(report_id)
        )
    except (openpaper.backend.models.scannerdb.ScanReport.DoesNotExist, ValueError):
        raise django.http.Http404('Report {} not found'.format(report_id))
    return report


@django.views.decorators.http.require_GET
def report(request, report_id):
    report_db = get_report(report_id)
    ctx = dict(DEFAULT_CTX)
    ctx['selected_report'] = report_db
    ctx['selected_scanner'] = report_db.scanner
    ctx['selected_manufacturer'] = ctx['selected_scanner'].manufacturer
    ctx['manufacturers'] = (
        openpaper.backend.models.scannerdb.ScannerManufacturer.objects.filter(
            moderated=True
        ).order_by('name')
    )
    ctx['scanners'] = openpaper.backend.models.scannerdb.Scanner.objects.filter(
        moderated=True, manufacturer=ctx['selected_manufacturer']
    ).order_by('name')
    ctx['reports'] = openpaper.backend.models.scannerdb.ScanReport.objects.filter(
        moderated=True, scanner=ctx['selected_scanner']
    ).order_by('-creation_date')

    ctx['admin_comment_form'] = forms.AdminCommentForm()

    scanner_type_img = get_scanner_img(ctx['selected_scanner'])

    attachments = openpaper.backend.models.scannerdb.ScanReportAttachment.objects.filter(
        report=report_db
    )

    report_tree = report_db.get_tree(request)

    # Things I missed in the migration ...
    if 'system' in report_tree:
        r_sys = report_tree['system']
        if 'sys_type' in r_sys:
            r_sys['sys_os_name'] = r_sys['sys_type']
        if 'sys_python' in r_sys:
            r_sys['sys_software_python'] = r_sys['sys_python']
        if 'sys_platform_short' in r_sys:
            r_sys['sys_software_system'] = r_sys['sys_platform_short']
        if 'sys_platform_detailed' in r_sys:
            r_sys['sys_platform_distribution'] = r_sys['sys_platform_detailed']
        if 'sys_platform_release' in r_sys:
            r_sys['sys_software_release'] = r_sys['sys_platform_release']
        if 'sys_system' in r_sys:
            r_sys['sys_software_system'] = r_sys['sys_system']
        if 'sys_nb_cpus' in r_sys:
            r_sys['sys_cpu_count'] = r_sys['sys_nb_cpus']
        if 'sys_proc' in r_sys:
            r_sys['sys_platform_processor'] = r_sys['sys_proc']
        if 'sys_machine' in r_sys:
            r_sys['sys_platform_machine'] = r_sys['sys_machine']
        if 'sys_mem' in r_sys:
            r_sys['sys_platform_mem'] = r_sys['sys_mem']
        if 'versions' in r_sys:
            r_ver = r_sys['versions']
            if 'ironscanner' in r_ver:
                r_ver['test_program'] = r_ver['ironscanner']

    successful = False
    if 'scantest' in report_tree and 'successful' in report_tree['scantest']:
        successful = bool(report_tree['scantest']['successful'])

    ctx.update({
        'report_db': report_db,
        'report': report_tree,
        'scanner_type_img': scanner_type_img,
        # we don't store boolean in the DB, only integer --> for proper
        # display,we must convert the successful status to a boolean
        'successful': successful,
        'attachments': attachments,
    })
    return django.shortcuts.render(request, 'scandb_report.html', ctx)


@django.views.decorators.http.require_POST
def delete_report(request, report_id):
    if not request.user.is_authenticated or not request.user.is_superuser:
        return django.http.HttpResponseForbidden()
    try:
        report = openpaper.backend.models.scannerdb.ScanReport.objects.get(
            id=int(report_id)
        )
    except (openpaper.backend.models.scannerdb.ScanReport.DoesNotExist, ValueError):
        raise django.http.Http404('Report {} not found'.format(report_id))
    report.delete()
    return django.http.HttpResponseRedirect(
        redirect_to=django.urls.reverse("scannerdb:home")
    )


@django.views.decorators.http.require_POST
def validate_report(request, report_id):
    if not request.user.is_authenticated or not request.user.is_superuser:
        return django.http.HttpResponseForbidden()
    try:
        report = openpaper.backend.models.scannerdb.ScanReport.objects.get(
            id=int(report_id)
        )
    except (openpaper.backend.models.scannerdb.ScanReport.DoesNotExist, ValueError):
        raise django.http.Http404(f'Report {report_id} not found')

    report.moderated = True
    report.save()
    report.scanner.moderated = True
    report.scanner.save()
    report.scanner.manufacturer.moderated = True
    report.scanner.manufacturer.save()

    openpaper.backend.models.scannerdb.invalidate_cache()

    return django.http.HttpResponseRedirect(
        redirect_to=django.urls.reverse("scannerdb:report", args=[report.id])
    )


@django.views.decorators.http.require_POST
def comment_report(request, report_id):
    if (not request.user.is_authenticated or
            not request.user.is_superuser):
        return django.http.HttpResponseForbidden()
    try:
        report = openpaper.backend.models.scannerdb.ScanReport.objects.get(
            id=int(report_id)
        )
    except (openpaper.backend.models.scannerdb.ScanReport.DoesNotExist, ValueError):
        raise django.http.Http404('Report {} not found'.format(report_id))

    form = forms.AdminCommentForm(request.POST)
    if not form.is_valid():
        return django.http.HttpResponseForbidden()

    report.moderated = False
    report.admin_comment = form.cleaned_data['reason']
    report.save()

    return django.http.HttpResponseRedirect(
        redirect_to=django.urls.reverse("scannerdb:report", args=[report.id])
    )


@django.views.decorators.http.require_GET
def report_traces_colorized(request, report_id, attachment_id):
    report = django.shortcuts.get_object_or_404(
        openpaper.backend.models.scannerdb.ScanReport.objects,
        pk=int(report_id)
    )
    attachment = django.shortcuts.get_object_or_404(
        openpaper.backend.models.scannerdb.ScanReportAttachment.objects,
        pk=int(attachment_id)
    )

    if report != attachment.report:
        return django.http.HttpResponseForbidden()
    if attachment.mime_type != "text/plain":
        return django.http.HttpResponseForbidden()

    traces = []
    with attachment.attachment.open("rb") as fd:
        traces = fd.read()
        traces = traces.decode("UTF-8", errors="replace")
        traces = traces.split("\n")
    for (idx, line) in enumerate(traces):
        traces[idx] = line.strip()

    previous = None
    chunk = []
    colorized = []
    for trace in traces:
        current = trace.split(" ")[0]
        if len(current) > 0 and current[0] == '[':
            current = current[1:]
        if len(current) > 0 and current[-1] == ']':
            current = current[:-1]

        if previous is not None and previous != current:
            color = "black"
            if previous == "DEBUG":
                color = "gray"
            elif previous == "WARNING":
                color = "#FFA500"  # orange
            elif previous == "ERROR":
                color = "red"
            colorized.append({
                'type': previous,
                'color': color,
                'lines': chunk,
            })
            chunk = []

        chunk.append(trace)
        previous = current

    ctx = {
        'report_db': report,
        'traces': colorized,
    }

    return django.shortcuts.render(request, 'scandb_traces.html', ctx)


@django.views.decorators.http.require_POST
def analyze_report(request, report_id):
    if not request.user.is_authenticated or not request.user.is_superuser:
        return django.http.HttpResponseForbidden()
    try:
        report = openpaper.backend.models.scannerdb.ScanReport.objects.get(
            id=int(report_id)
        )
    except (openpaper.backend.models.scannerdb.ScanReport.DoesNotExist, ValueError):
        raise django.http.Http404(f'Report {report_id} not found')

    attachments = openpaper.backend.models.scannerdb.ScanReportAttachment.objects.filter(
        report=report
    )
    for attachment in attachments:
        if attachment.is_analysis_result():
            attachment.unlink()

    attachments = openpaper.backend.models.scannerdb.ScanReportAttachment.objects.filter(
        report=report
    )
    for attachment in attachments:
        attachment.analyze()

    return django.http.HttpResponseRedirect(
        redirect_to=django.urls.reverse("scannerdb:report", args=[report.id])
    )


@django.views.decorators.http.require_POST
def analyze_all(request):
    if not request.user.is_authenticated or not request.user.is_superuser:
        return django.http.HttpResponseForbidden()

    attachments = openpaper.backend.models.scannerdb.ScanReportAttachment.objects.all()
    for attachment in attachments:
        if attachment.is_analysis_result():
            attachment.unlink()

    attachments = openpaper.backend.models.scannerdb.ScanReportAttachment.objects.all()
    for attachment in attachments:
        attachment.analyze()

    return django.http.HttpResponseRedirect(
        redirect_to=django.urls.reverse("scannerdb:home")
    )
