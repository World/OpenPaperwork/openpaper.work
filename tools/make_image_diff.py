#!/usr/bin/env python3

import sys
import PIL.Image


def main():
    reference_image = sys.argv[1] if len(sys.argv) > 1 else None
    outline_image = sys.argv[2] if len(sys.argv) > 2 else None

    if outline_image is None:
        print("Syntax:")
        print(
            "  {} <reference image> <outline image>".format(
                sys.argv[0]
            )
        )
        sys.exit(1)

    reference_image = PIL.Image.open(reference_image)
    reference_image = reference_image.convert("RGB")

    outline_image = PIL.Image.open(outline_image)
    outline_image = outline_image.convert("RGB")

    if reference_image.size != outline_image.size:
        print("Image sizes don't match: {} VS {}".format(
            reference_image.size, outline_image.size
        ))
        sys.exit(1)

    size = reference_image.size

    print("x,y")

    for x in range(size[0]):
        for y in range(size[1]):
            a = reference_image.getpixel((x, y))
            b = outline_image.getpixel((x, y))
            if a != b:
                print("{},{}".format(x, y))


if __name__ == "__main__":
    main()
