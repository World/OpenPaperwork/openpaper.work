#!/usr/bin/env python3

"""
Download all valid scans from all reports.
Useful to figure out what could be valid material for IA / Computer Vision.
Also useful to find quickly reports that were marked successful when they
actually were not.
"""

import os
import sys

import requests


def main():
    report_url = sys.argv[1] if len(sys.argv) > 1 else None
    out_dir = sys.argv[2] if len(sys.argv) > 2 else None
    if report_url is None or out_dir is None:
        print("Syntax:")
        print("  {} <report_url> <output directory>".format(sys.argv[0]))
        return

    print("GET {} ...".format(report_url))
    report = requests.get(report_url)
    report = report.json()

    resolution = report.get(
        'data', {}
    ).get(
        'scantest', {}
    ).get(
        'config', {}
    ).get('resolution', None)
    if resolution is None:
        print("WARNING: Resolution is missing")
    elif resolution < 75:
        print("WARNING: Resolution too low: {}dpi".format(resolution))
        resolution = None

    os.makedirs(out_dir, mode=0o700, exist_ok=True)

    for attachment_url in report['attachments']:
        print("")
        print("GET {} ...".format(attachment_url))
        attachment = requests.get(attachment_url)
        attachment = attachment.json()

        if (attachment['mime_type'].startswith("image/") and
                attachment['file_name'].startswith("image_")):
            file_name = attachment['file_name'].rsplit('.', 1)
            file_name = "{}_{}dpi.{}".format(
                file_name[0], resolution, file_name[1]
            )
        else:
            file_name = attachment['file_name']

        print("GET {} ==> {} ...".format(
            attachment['attachment'], file_name
        ))
        r = requests.get(attachment['attachment'])
        out_path = os.path.join(out_dir, file_name)
        with open(out_path, 'wb') as fd:
            fd.write(r.content)

    print("All done !")


if __name__ == "__main__":
    main()
