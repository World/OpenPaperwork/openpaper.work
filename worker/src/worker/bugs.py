from typing import List

import logging
import re
import threading

import django.db.transaction
import kneed
import matplotlib.pyplot
import numpy
import sklearn.cluster
import sklearn.decomposition
import sklearn.feature_extraction.text
import sklearn.preprocessing

import openpaper.beacon.models


LOGGER = logging.getLogger(__name__)
MAX_ATTACHMENTS = 1000
MAX_CLUSTERS = 1500
NB_CLUSTERS = 200
VECTOR_LENGTH = 150
MIN_SIMILARITIES = 0.01
ELBOW_THRESHOLD = 0.01
ELBOW_FACTOR = 1


class MemTraceback:
    TRACEBACK_RES = [
        re.compile(r'^.*=== caught exception in promise ===$'),
        re.compile(r'^Traceback (most recent call last)$'),
        re.compile(r'^\s*File ".*", line \d+, in .+$'),
        re.compile(r'^.+promise.func=.+$'),
        re.compile(r'^.+promise.args=.+$'),
        re.compile(r'^.+promise.kwargs=.+$'),
        re.compile(r'^.+promise.parent=.+$'),
        re.compile(r'^.+promise.parent_promise_return=.+$'),
        re.compile(r'^.+=== Promise was created by ===$'),
        re.compile(r'^.+=== Promise was queued by ===$'),
        re.compile(r'^.+==========================$'),
        re.compile(r'^.+During handling of the above exception, another exception occurred:$'),
        re.compile(r'^.+uncaught exception.*$', re.IGNORECASE),
        # 31.223438 [ERROR ] [openpaperwork_core.work_queue.default]
        # 5: C:/msys64/mingw64/lib/python3.11/site-packages/paperwork_backend/sync/doctracker.py:
        # L  443: <lambda>
        re.compile(r'^.*\[.*\] \[.*\] [0-9 ]+: .*: L[0-9 ]+: .+$'),
    ]
    TRACEBACK_KEEPALIVE = 4

    def __init__(
            self,
            log_file,
            line_traceback_start: int,
            line_traceback_end: int):
        self.log_file = log_file
        self.line_traceback_start = line_traceback_start
        self.line_traceback_end = line_traceback_end
        self.cluster = None

    def __eq__(self, o):
        return (
            self.log_file.db_attachment.body.path == o.log_file.db_attachment.body.path
            and self.line_traceback_start == o.line_traceback_start
            and self.line_traceback_end == o.line_traceback_end
        )

    def __hash__(self):
        return (
            hash(self.log_file.db_attachment.body.path)
            ^ hash(self.line_traceback_start)
            ^ hash(self.line_traceback_end)
        )

    @classmethod
    def from_db(cls, db_traceback):
        return cls(
            LogFile(db_traceback.log_file),
            db_traceback.line_start,
            db_traceback.line_end,
        )

    def insert(self):
        openpaper.beacon.models.Traceback(
            log_file=self.log_file.db_attachment,
            line_start=self.line_traceback_start,
            line_end=self.line_traceback_end,
            cluster=self.cluster.db_cluster,
        ).save()

    def read(self):
        with open(self.log_file.db_attachment.body.path, "r") as fd:
            lines = fd.readlines()
        lines = lines[self.line_traceback_start:self.line_traceback_end]
        lines = [line.rstrip() for line in lines]
        return "\n".join(lines)

    @classmethod
    def _match_line(cls, line):
        for r in cls.TRACEBACK_RES:
            if r.match(line):
                return True
        return False

    @classmethod
    def extract_from_lines(cls, log_file, lines: List[str]):
        traceback_start = None
        traceback_keepalive = cls.TRACEBACK_KEEPALIVE

        for (line_idx, line) in enumerate(lines):
            if line.strip() == "":
                continue
            if cls._match_line(line):
                if traceback_start is None:
                    traceback_start = max(line_idx - 1, 0)
                traceback_keepalive = cls.TRACEBACK_KEEPALIVE
            else:
                if traceback_start is not None:
                    if traceback_keepalive <= 0:
                        traceback_end = line_idx
                        LOGGER.debug(
                            "Traceback found: %s L%d-L%d",
                            log_file.db_attachment, traceback_start + 1, traceback_end + 1
                        )
                        yield cls(log_file, traceback_start, traceback_end)
                        traceback_start = None
                    traceback_keepalive -= 1


class LogFile:
    def __init__(self, db_attachment: openpaper.beacon.models.BugReportAttachment):
        self.db_attachment = db_attachment

    def extract_tracebacks(self):
        with open(self.db_attachment.body.path, "r") as fd:
            content = fd.readlines()
        content = [line.strip() for line in content]
        self.tracebacks = list(MemTraceback.extract_from_lines(self, content))


class MemBugCluster:
    def __init__(self, db_cluster=None):
        self.db_cluster = db_cluster
        self.tracebacks = []
        self.old_cluster = None
        self.new_cluster = None


class Corpus:
    def __init__(self):
        self.bug_reports = []
        self.log_files = []
        self.tracebacks = []

    def load(self):
        self.log_files = [
            LogFile(db_attachment)
            for db_attachment in (
                openpaper.beacon.models.BugReportAttachment
                .objects
                .filter(bug_report__app_name="paperwork")
                .filter(file_name__contains="_logs.txt")
                .order_by("-bug_report__creation_date")
                [:MAX_ATTACHMENTS]
            )
        ]
        LOGGER.info("%d log files found", len(self.log_files))

        for log_file in self.log_files:
            log_file.extract_tracebacks()
            self.tracebacks += log_file.tracebacks
        LOGGER.info("%d tracebacks found", len(self.tracebacks))

    def iter_traceback_texts(self):
        for traceback in self.tracebacks:
            yield traceback.read()


def find_elbow_threshold(ssd):
    LOGGER.info("SSD: %s", ssd)
    percentage_changes = numpy.diff(ssd) / ssd[:-1]
    percentage_changes = numpy.absolute(percentage_changes)
    LOGGER.info("Changes: %s", percentage_changes)
    # Add 1 to account for the shift due to numpy.diff
    ar = numpy.where(percentage_changes < ELBOW_THRESHOLD)
    if len(ar) <= 0 or len(ar[0]) <= 0:
        return len(ssd) - 1
    elbow_index = ar[0][0] + 1
    return min(int(elbow_index * ELBOW_FACTOR), len(ssd) - 1)


def find_elbow_second_derivative(ssd):
    # Calculate the second derivative
    second_derivative = numpy.diff(numpy.diff(ssd))
    # Find the index of minimum absolute value and add 1 for offset
    elbow_index = numpy.argmin(numpy.abs(second_derivative)) + 1
    return elbow_index


def find_elbow_kneed(ssd):
    kneedle = kneed.KneeLocator(
        range(0, len(ssd)),
        ssd,
        curve="convex",
        direction="decreasing",
    )
    LOGGER.info(
        "Kneedle: %s => %f ; %f",
        ssd,
        kneedle.knee,
        kneedle.elbow,
    )
    return min(int(kneedle.elbow * ELBOW_FACTOR), len(ssd) - 1)


class BugClustersComputation:
    running = False

    def _load_clusters(self):
        LOGGER.info("Loading clusters from DB ...")

        db_clusters = openpaper.beacon.models.BugCluster.objects.all()
        db_tracebacks = openpaper.beacon.models.Traceback.objects.all()

        clusters = {
            db_cluster: MemBugCluster(db_cluster)
            for db_cluster in db_clusters
        }
        tracebacks = {
            db_traceback: MemTraceback.from_db(db_traceback)
            for db_traceback in db_tracebacks
        }

        for db_traceback in db_tracebacks:
            clusters[db_traceback.cluster].tracebacks.append(tracebacks[db_traceback])
            tracebacks[db_traceback].cluster = clusters[db_traceback.cluster]

        LOGGER.info("%d clusters loaded from DB", len(clusters))
        return clusters.values()

    def _compute_clusters(self):
        LOGGER.info("Recomputing bug clusters")

        LOGGER.info("Loading corpus ...")
        corpus = Corpus()
        corpus.load()
        LOGGER.info("Corpus loaded")

        LOGGER.info("Vectorizing tracebacks ...")
        vectorizer = sklearn.feature_extraction.text.TfidfVectorizer()
        # vectorizer = sklearn.feature_extraction.text.HashingVectorizer(n_features=2**16)
        x_corpus = vectorizer.fit_transform(corpus.iter_traceback_texts())
        x_corpus = x_corpus.toarray()
        # x_corpus = sklearn.preprocessing.StandardScaler().fit_transform(x_corpus)
        LOGGER.info("Tracebacks vectorized: %s", x_corpus.shape)

        LOGGER.info("Reducing vectors to %d elements ...", VECTOR_LENGTH)
        decomp = sklearn.decomposition.TruncatedSVD(n_components=VECTOR_LENGTH, random_state=42)
        # decomp = sklearn.decomposition.PCA(n_components=VECTOR_LENGTH, random_state=42)
        x = decomp.fit_transform(x_corpus)
        del x_corpus  # release this big ass chunk of RAM
        # elbow = find_elbow_second_derivative(decomp.explained_variance_ratio_)
        LOGGER.info("Vectors reduced: %s", x.shape)
        matplotlib.pyplot.plot(
            range(1, len(decomp.explained_variance_ratio_) + 1),
            decomp.explained_variance_ratio_,
            'bx-'
        )
        matplotlib.pyplot.xlabel('axis')
        matplotlib.pyplot.ylabel('explained variance ratio')
        matplotlib.pyplot.title("Decomposition")
        e = int(find_elbow_kneed(decomp.explained_variance_ratio_))
        matplotlib.pyplot.bar(e, max(decomp.explained_variance_ratio_))
        matplotlib.pyplot.text(e, max(decomp.explained_variance_ratio_), "kneed")
        e = int(find_elbow_second_derivative(decomp.explained_variance_ratio_))
        matplotlib.pyplot.bar(e, max(decomp.explained_variance_ratio_))
        matplotlib.pyplot.text(e, max(decomp.explained_variance_ratio_), "deriv")
        e = int(find_elbow_threshold(decomp.explained_variance_ratio_))
        matplotlib.pyplot.bar(e, max(decomp.explained_variance_ratio_))
        matplotlib.pyplot.text(e, max(decomp.explained_variance_ratio_), "threshold")
        matplotlib.pyplot.savefig("/var/www/uploads/bug_clusters_decomp.png")
        matplotlib.pyplot.close()

        LOGGER.info("Clustering ...")
        all_kmeans = []
        for k in range(1, MAX_CLUSTERS):
            LOGGER.info("Trying with %d clusters ...", k)
            kmeans = sklearn.cluster.KMeans(n_clusters=k, random_state=42)
            r = numpy.unique(kmeans.fit_predict(x))
            if len(r) < k:
                LOGGER.info("Only %d clusters found", len(r))
                break
            all_kmeans.append((k, kmeans.inertia_))

        kmeans = [x[1] for x in all_kmeans]

        LOGGER.info("Plotting Kmeans ...")
        matplotlib.pyplot.plot(range(1, len(kmeans) + 1), kmeans, 'bx-')
        matplotlib.pyplot.xlabel('k')
        matplotlib.pyplot.ylabel('Sum_of_squared_distances')
        matplotlib.pyplot.title("KMeans's squared distances for each n_clusters")
        e = int(find_elbow_kneed(kmeans))
        matplotlib.pyplot.bar(e, max(kmeans), width=10)
        matplotlib.pyplot.text(e, max(kmeans), "kneed")
        e = int(find_elbow_second_derivative(kmeans))
        matplotlib.pyplot.bar(e, max(kmeans), width=10)
        matplotlib.pyplot.text(e, max(kmeans), "deriv")
        e = int(find_elbow_threshold(kmeans))
        matplotlib.pyplot.bar(e, max(kmeans), width=10)
        matplotlib.pyplot.text(e, max(kmeans), "threshold")
        matplotlib.pyplot.savefig("/var/www/uploads/bug_clusters_clustering.png")
        matplotlib.pyplot.close()

        # elbow = find_elbow_threshold(kmeans)
        # elbow = find_elbow_kneed(kmeans)
        # elbow = find_elbow_second_derivative(kmeans)
        elbow = min(NB_CLUSTERS, len(all_kmeans)) - 1
        (n_clusters, kmeans_inertia) = all_kmeans[elbow]
        LOGGER.info("Elbow n_clusters=%d (inertia=%f)", n_clusters, kmeans_inertia)

        kmeans = sklearn.cluster.KMeans(n_clusters=n_clusters, random_state=42)
        cluster_affectations = kmeans.fit_predict(x)
        clusters = [MemBugCluster() for _ in range(0, n_clusters)]

        for (traceback_idx, traceback) in enumerate(corpus.tracebacks):
            n_cluster = cluster_affectations[traceback_idx]

            cluster = clusters[int(n_cluster)]
            cluster.tracebacks.append(traceback)
            traceback.cluster = cluster

        LOGGER.info("%d clusters found", len(clusters))
        for (cluster_idx, cluster) in enumerate(clusters):
            LOGGER.debug("Cluster #%d: %d tracebacks", cluster_idx, len(cluster.tracebacks))

        LOGGER.info("Plotting cluster sizes ...")
        max_tracebacks = max((len(cluster.tracebacks) for cluster in clusters))
        cluster_sizes = [0] * (max_tracebacks + 1)
        for cluster in clusters:
            cluster_sizes[len(cluster.tracebacks)] += 1
        matplotlib.pyplot.plot(
            range(1, max_tracebacks + 1),
            cluster_sizes[1:],
            'bx-'
        )
        matplotlib.pyplot.xlabel('nb tracebacks')
        matplotlib.pyplot.ylabel('nb clusters')
        matplotlib.pyplot.title("Cluster sizes")
        matplotlib.pyplot.savefig("/var/www/uploads/bug_clusters_cluster_sizes.png")
        matplotlib.pyplot.close()

        LOGGER.info("Bug clusters recomputed")
        return clusters

    def _compute_similarity(self, old_cluster, new_cluster):
        old_tracebacks = set(old_cluster.tracebacks)
        new_tracebacks = set(new_cluster.tracebacks)
        return (
            len(old_tracebacks.intersection(new_tracebacks))
            / ((len(old_tracebacks) + len(new_tracebacks)) / 2)
        )

    def _connect_old_and_new_clusters(self, old_clusters, new_clusters):
        old_clusters = list(old_clusters)
        new_clusters = list(new_clusters)
        LOGGER.info(
            "Computing similarities between old and new clusters (%dx%d=%d)...",
            len(old_clusters), len(new_clusters), len(old_clusters) * len(new_clusters)
        )
        similarities = [
            [
                self._compute_similarity(old_cluster, new_cluster)
                for new_cluster in new_clusters
            ]
            for old_cluster in old_clusters
        ]
        similarities = numpy.array(similarities)
        LOGGER.info("Connecting old and new clusters ...")
        while len(old_clusters) > 0 and len(new_clusters) > 0:
            best = similarities.argmax()
            best = numpy.unravel_index(best, similarities.shape)
            if similarities[best[0], best[1]] <= MIN_SIMILARITIES:
                break

            similarities = numpy.delete(similarities, best[0], axis=0)
            similarities = numpy.delete(similarities, best[1], axis=1)

            old_cluster = old_clusters[best[0]]
            new_cluster = new_clusters[best[1]]

            old_cluster.new_cluster = new_cluster
            new_cluster.old_cluster = old_cluster
            old_clusters.remove(old_cluster)
            new_clusters.remove(new_cluster)
            LOGGER.info("Connecting: %d remaining", min(len(old_clusters), len(new_clusters)))
        LOGGER.info("Old and new clusters connected")

    @staticmethod
    def _def_int(v: str, default: int) -> int:
        try:
            return int(v)
        except ValueError:
            return default

    def _version_to_tuple(self, ver):
        ver = ver.split(".")
        return (
            self._def_int(ver[0], 0),
            self._def_int(ver[1], 0) if len(ver) > 1 else 0,
            self._def_int(ver[2], 0) if len(ver) > 2 else 0,
        )

    def _get_latest_paperwork_version(self, cluster):
        attachments = {
            traceback.log_file.db_attachment
            for traceback in cluster.tracebacks
        }
        versions = {
            attachment.bug_report.app_version
            for attachment in attachments
        }
        versions = {
            self._version_to_tuple(version)
            for version in versions
        }
        version = max(versions)
        version = [str(x) for x in version]
        version = ".".join(version)
        return version

    def _get_latest_report(self, cluster):
        attachments = {
            traceback.log_file.db_attachment
            for traceback in cluster.tracebacks
        }
        reports = {
            attachment.bug_report
            for attachment in attachments
        }
        return max(reports, key=lambda r: r.creation_date)

    def _add_cluster(self, new_cluster):
        LOGGER.info("New cluster with %d tracebacks", len(new_cluster.tracebacks))
        new_cluster.db_cluster = openpaper.beacon.models.BugCluster(
            read=False,
            solved=False,
            latest_paperwork_version=self._get_latest_paperwork_version(new_cluster),
            latest_report=self._get_latest_report(new_cluster),
            nb_tracebacks=len(new_cluster.tracebacks),
        )
        new_cluster.db_cluster.save()
        for traceback in new_cluster.tracebacks:
            traceback.insert()

    def _diff_cluster(self, old_cluster, new_cluster):
        LOGGER.info(
            "Diff between old and new cluster (before=%d, after=%d)",
            len(old_cluster.tracebacks),
            len(new_cluster.tracebacks),
        )
        new_cluster.db_cluster = old_cluster.db_cluster
        new_cluster.db_cluster.latest_paperwork_version = (
            self._get_latest_paperwork_version(new_cluster)
        )
        new_cluster.db_cluster.latest_report = self._get_latest_report(new_cluster)
        new_cluster.nb_tracebacks = len(new_cluster.tracebacks)
        new_cluster.db_cluster.save()
        for traceback in new_cluster.tracebacks:
            traceback.insert()

    def _del_cluster(self, old_cluster):
        LOGGER.info("Old cluster with %d tracebacks has disappeared", len(old_cluster.tracebacks))
        old_cluster.db_cluster.delete()

    def __do(self):
        old_clusters = self._load_clusters()
        new_clusters = self._compute_clusters()

        new_clusters = [
            cluster
            for cluster in new_clusters
            if len(cluster.tracebacks) > 0
        ]

        self._connect_old_and_new_clusters(old_clusters, new_clusters)

        with django.db.transaction.atomic():
            openpaper.beacon.models.Traceback.objects.all().delete()

            for new_cluster in new_clusters:
                if new_cluster.old_cluster is None:
                    self._add_cluster(new_cluster)
                else:
                    self._diff_cluster(new_cluster.old_cluster, new_cluster)

            for old_cluster in old_clusters:
                if old_cluster.new_cluster is None:
                    self._del_cluster(old_cluster)

        LOGGER.info("All done !")

    def _do(self):
        try:
            self.__do()
        finally:
            self.running = False

    def do(self):
        if self.running:
            return
        self.running = True
        t = threading.Thread(target=self._do)
        t.start()
