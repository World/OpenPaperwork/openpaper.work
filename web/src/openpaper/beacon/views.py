import base64
import datetime
import json
import logging
import re
import secrets
import string

import GeoIP
import ipware

from django.contrib.admin.views.decorators import staff_member_required
from django.core import serializers
from django.core.files.base import ContentFile
from django.core.mail import mail_admins
from django.db import transaction
from django.db.models import Count
from django.db.models import Sum
from django.http import Http404
from django.http import HttpResponse
from django.http import HttpResponseRedirect
from django.http import JsonResponse
from django.shortcuts import render
from django.urls import reverse
from django.utils.translation import gettext_lazy as _
from django.views.decorators.cache import cache_page
from django.views.decorators.csrf import csrf_exempt
from django.views.decorators.http import require_GET
from django.views.decorators.http import require_POST
import django.db

import openpaper.backend.msgqueue
import openpaper.backend.util

from .models import (
    BugCluster,
    BugReport,
    BugReportAttachment,
    Traceback,
    Statistics,
    StatsHistory,
    StatsHistoryOs,
    StatsHistoryVersion
)

LATEST_PAPERWORK_VERSION = "2.2.5"

STATS_MAX_DAYS = 40
STATS_MIN_DOCS = 50

LOGGER = logging.getLogger(__name__)

# Used on data automatically sent from Paperwork to avoid any possible XSS
STRICT_FILTER = re.compile("[^a-zA-Z0-9._ ():'-]")

GEOIP4_DB = geo = GeoIP.open(
    "/usr/share/GeoIP/GeoIP.dat", GeoIP.GEOIP_STANDARD
)
GEOIP6_DB = geo = GeoIP.open(
    "/usr/share/GeoIP/GeoIPv6.dat", GeoIP.GEOIP_STANDARD
)


UNKNOWN = "Unknown"


@require_GET
def latest(request):
    return JsonResponse({
        "paperwork": {
            "posix": LATEST_PAPERWORK_VERSION,
            "nt": LATEST_PAPERWORK_VERSION,
        },
    })


def cleanup():
    LOGGER.info(
        "Cleaning up date before: %s",
        str(datetime.date.today() - datetime.timedelta(days=STATS_MAX_DAYS))
    )
    Statistics.objects.filter(
        last_mod_date__lt=(
            datetime.date.today() - datetime.timedelta(days=STATS_MAX_DAYS)
        )
    ).delete()


def must_compile_stats():
    try:
        history = StatsHistory.objects.order_by("-creation_date")
        last = history[0]
        diff = datetime.datetime.now(
            datetime.timezone.utc
        ) - last.creation_date
        r = diff.days > 0
        LOGGER.info("must_compile_stats() ? %s", r)
        return r
    except (StatsHistory.DoesNotExist, IndexError):
        LOGGER.info("No stat compilations yet.")
        return True


def compile_stats():
    if not must_compile_stats():
        return

    nb_instances = Statistics.objects.filter(
        nb_documents__gte=STATS_MIN_DOCS
    ).count()
    max_docs = Statistics.objects.order_by('-nb_documents')[0].nb_documents
    total_docs = Statistics.objects.aggregate(
        Sum('nb_documents')
    )['nb_documents__sum']

    LOGGER.info(
        "nb_instances=%s, max_docs=%s, total_docs=%s",
        nb_instances, max_docs, total_docs
    )
    new_history = StatsHistory(
        nb_instances=nb_instances, max_docs=max_docs, total_docs=total_docs
    )
    new_history.save()

    versions = Statistics.objects.values('paperwork_version').annotate(Count(
        'paperwork_version'
    ))
    for version in versions:
        LOGGER.info("Versions: %s", version)
        h_version = StatsHistoryVersion(
            history=new_history,
            paperwork_version=version['paperwork_version'],
            count=version['paperwork_version__count']
        )
        h_version.save()

    oses = Statistics.objects.values('os_name').annotate(Count('os_name'))
    for o in oses:
        LOGGER.info("OS: %s", o)
        h_os = StatsHistoryOs(
            history=new_history,
            os_name=o['os_name'],
            count=o['os_name__count']
        )
        h_os.save()


def _strict_filter(string):
    if isinstance(string, int):
        return string
    return STRICT_FILTER.sub("", string)


def _get_country_from_ip(ip):
    if ip is None:
        return None
    if ":" in ip:
        return GEOIP6_DB.country_code_by_addr_v6(ip)
    else:
        return GEOIP4_DB.country_code_by_addr(ip)


@csrf_exempt
@require_POST
def post_statistics(request):
    django.db.close_old_connections()  # workaround for 'mysql has gone away'

    stats = request.POST['statistics']
    stats = json.loads(stats)
    stats = {
        k: _strict_filter(v)
        for (k, v) in stats.items()
    }

    LOGGER.info("Getting statistics")
    # We don't need the exact Paperwork version
    paperwork_version = stats['paperwork_version'].split("-", 1)[0]
    if ".dev" in paperwork_version:
        paperwork_version = paperwork_version.split(".dev", 1)[0]
        paperwork_version = paperwork_version.split(".")
        paperwork_version[-1] = str(int(paperwork_version[-1]) - 1)
        paperwork_version = ".".join(paperwork_version)
    if ".post" in paperwork_version:
        paperwork_version = paperwork_version.split(".post", 1)[0]
    paperwork_version = paperwork_version[:30]
    platform_processor = stats['platform_processor'][:14]

    if "flatpak" in stats.get('os_name'):
        stats['platform_distribution'] = 'Flatpak'
        stats['os_name'] = 'Linux'
    else:
        stats['os_name'] = stats.get('os_name').replace('nt', 'Windows')
        stats['os_name'] = stats.get('os_name').replace('posix', 'Linux')
        stats['platform_distribution'] = (
            stats.get('platform_distribution').split("'")[1]
        )

    stats['platform_architecture'] = (
        stats.get('platform_architecture').split("'")[1]
    )
    (ip, is_routable) = ipware.get_client_ip(request)
    country = _get_country_from_ip(ip)
    LOGGER.info("GeoIP: %s -> %s", ip, country)

    try:
        stats_obj = Statistics.objects.get(uuid=stats['uuid'])
        LOGGER.info("Statistics post: UUID=%s", stats['uuid'])
        stats_obj.paperwork_version = paperwork_version
        stats_obj.platform_processor = platform_processor
        for field in ['nb_documents',
                      'os_name', 'platform_architecture',
                      'platform_distribution']:
            LOGGER.info("%s=%s", field, stats.get(field))
            setattr(stats_obj, field, stats.get(field))
    except Statistics.DoesNotExist:
        stats_obj = Statistics(
            uuid=stats['uuid'],
            paperwork_version=paperwork_version,
            nb_documents=stats.get('nb_documents'),
            os_name=stats.get('os_name'),
            platform_architecture=stats.get('platform_architecture'),
            platform_processor=platform_processor,
            platform_distribution=stats.get('platform_distribution'),
            country=country,
        )
    stats_obj.save()

    cleanup()

    with transaction.atomic():
        compile_stats()

    return HttpResponse("OK")


def get_ranges(thresholds):
    for idx in range(0, len(thresholds) - 1):
        yield (
            "{}-{}".format(thresholds[idx], thresholds[idx + 1] - 1),
            (thresholds[idx], thresholds[idx + 1] - 1)
        )
    yield (
        "≥{}".format(thresholds[-1]),
        (thresholds[-1], None)
    )


class NoopFilterFactory(object):
    field = ''
    nice_name = _("All")

    def make(self, value, previous):
        return NoopFilter()


class NoopFilter(object):
    LINKABLE = False
    previous = None
    factory = NoopFilterFactory()

    def __init__(self, nice_name=None, count=-1):
        self.count = count
        self.nice_name = nice_name
        self.value = nice_name if nice_name is not None else _("All")

    def filter(self, queryset):
        return queryset

    def get_other_filter_names(self):
        r = set(KNOWN_FILTERS.keys())
        return r

    def __str__(self):
        return ''


class BaseFilter:
    def __init__(self, nice_name):
        self.nice_name = nice_name

    def get_other_filter_names(self):
        filters = self.previous.get_other_filter_names()
        filters.remove(self.factory.field)
        return filters

    def __str__(self):
        out = str(self.previous)
        if out != '':
            out += '|'
        out += "{}:{}".format(
            self.factory.field,
            self.value
        )
        return out


class ExactFilter(BaseFilter):
    LINKABLE = True

    def __init__(self, factory, value, previous, count=-1):
        super().__init__(value)
        self.factory = factory
        self.value = value
        self.previous = previous
        self.count = count

    def filter(self, queryset):
        value = self.value
        if value == str(UNKNOWN):
            value = None
        queryset = self.previous.filter(queryset)
        return queryset.filter(**{self.factory.field: value})


class RangeFilter(BaseFilter):
    LINKABLE = True

    def __init__(self, factory, min_val, max_val, previous, count=-1):
        self.factory = factory
        self.min_val = min_val
        self.max_val = max_val
        self.value = "{}-{}".format(self.min_val, self.max_val)
        super().__init__(self.value)
        self.previous = previous
        self.count = count

    def filter(self, queryset):
        queryset = self.previous.filter(queryset)
        return queryset.filter(**{
            self.factory.field + "__gte": self.min_val,
            self.factory.field + "__lte": self.max_val,
        })

    def get_other_filter_names(self):
        filters = self.previous.get_other_filter_names()
        filters.remove(self.factory.field)
        return filters

    def __str__(self):
        out = str(self.previous)
        if out != '':
            out += '|'
        out += "{}:{}".format(
            self.factory.field,
            self.value
        )
        return out


class GteFilter(BaseFilter):
    LINKABLE = True

    def __init__(self, factory, min_val, previous, count=-1):
        self.factory = factory
        self.min_val = min_val
        self.value = "≥{}".format(self.min_val)
        super().__init__(self.value)
        self.previous = previous
        self.count = count

    def filter(self, queryset):
        queryset = self.previous.filter(queryset)
        return queryset.filter(**{
            self.factory.field + "__gte": self.min_val,
        })


class ExactFilterFactory:
    def __init__(self, field, nice_name):
        self.field = field
        self.nice_name = nice_name

    def make(self, value, previous, count=-1):
        return ExactFilter(self, value, previous, count)


class RangeFilterFactory:
    def __init__(self, field, nice_name, thresholds):
        self.field = field
        self.nice_name = nice_name
        self.thresholds = thresholds

    def make(self, value, previous, count=-1):
        if "-" in value:
            (min_val, max_val) = value.split("-")
            min_val = int(min_val)
            max_val = int(max_val)
            return RangeFilter(self, min_val, max_val, previous, count)
        elif "≥" in value:
            (_, min_val) = value.split("≥")
            min_val = int(min_val)
            return GteFilter(self, min_val, previous, count)
        else:
            raise Http404("Unknown value type for range: " + str(value))


KNOWN_FILTERS = {
    "country": ExactFilterFactory(
        "country", _("Country")
    ),
    "nb_documents": RangeFilterFactory(
        "nb_documents", _("Number of documents"), [0, 5, 50, 1000]
    ),
    "os_name": ExactFilterFactory("os_name", _("OS")),
    "paperwork_version": ExactFilterFactory(
        "paperwork_version", _("Paperwork version")
    ),
    "platform_distribution": ExactFilterFactory(
        "platform_distribution", _("Distribution")
    ),
}


class NoopSplitter:
    field = ''

    def __init__(self, nice_name=None):
        self.nice_name = nice_name if nice_name is not None else _("All")

    def by_count(self, queryset):
        return [
            (self.nice_name, queryset.count()),
        ]

    def by_sum(self, field, queryset):
        return [
            (self.nice_name, queryset.aggregate(Sum(field))[field + "__sum"]),
        ]

    def total_count(self, queryset):
        return queryset.count()

    def total_sum(self, field, queryset):
        return queryset.aggregate(Sum(field))[field + "__sum"]

    def get_filter(self, value, previous, count=-1):
        previous.count = count
        return previous


class ExactSplitter:
    def __init__(self, field, nice_name):
        self.field = field
        self.nice_name = nice_name

    def by_count(self, queryset):
        for r in queryset.values(self.field).annotate(Count(self.field)):
            field_value = r[self.field]
            if field_value is None:
                field_value = str(UNKNOWN)
            yield (
                field_value,
                r[self.field + "__count"],
            )

    def by_sum(self, field, queryset):
        for r in queryset.values(self.field).annotate(Sum(field)):
            field_value = r[self.field]
            if field_value is None:
                field_value = str(UNKNOWN)
            yield (
                field_value,
                r[field + "__sum"],
            )

    def total_count(self, queryset):
        return queryset.count()

    def total_sum(self, field, queryset):
        return queryset.aggregate(Sum(field))[field + "__sum"]

    def get_filter(self, value, previous, count=-1):
        return KNOWN_FILTERS[self.field].make(value, previous, count=count)


class RangeSplitter:
    def __init__(self, field, nice_name, thresholds):
        self.field = field
        self.nice_name = nice_name
        self.thresholds = thresholds

    def by_count(self, queryset):
        for (name, (val_min, val_max)) in get_ranges(self.thresholds):
            q = queryset.filter(**{self.field + "__gte": val_min})
            if val_max is not None:
                q = q.filter(**{self.field + "__lte": val_max})
            yield (
                name,
                q.count()
            )

    def by_sum(self, field, queryset):
        for (name, (val_min, val_max)) in get_ranges(self.thresholds):
            q = queryset.filter(**{self.field + "__gte": val_min})
            if val_max is not None:
                q = q.filter(**{self.field + "__lte": val_max})
            if self.field == field:
                yield (name, q.aggregate(Sum(field))[field + '__sum'])
                continue
            q = q.values(self.field).annotate(Sum(field))
            if field + "__sum" not in q:
                continue
            q = q[field + "__sum"]
            yield (name, q)

    def total_count(self, queryset):
        return queryset.count()

    def total_sum(self, field, queryset):
        return queryset.aggregate(Sum(field))[field + "__sum"]

    def get_filter(self, value, previous, count=-1):
        return KNOWN_FILTERS[self.field].make(value, previous, count=count)


KNOWN_SPLITTERS = {
    "country": ExactSplitter(
        "country", _("Country")
    ),
    "nb_documents": RangeSplitter(
        "nb_documents", _("Number of documents"), [0, 5, 50, 1000]
    ),
    "os_name": ExactSplitter(
        "os_name", _("OS")
    ),
    "paperwork_version": ExactSplitter(
        "paperwork_version", _("Paperwork version")
    ),
    "platform_distribution": ExactSplitter(
        "platform_distribution", _("Distribution")
    ),
}


def parse_filters(strfilters):
    if strfilters == '':
        return NoopFilter()
    out = NoopFilter()
    for f in strfilters.split("|"):
        (k, v) = f.split(":", 1)
        if k not in KNOWN_FILTERS:
            raise Http404("Unknown filter key: " + k)
        out = KNOWN_FILTERS[k].make(v, previous=out)
    return out


def parse_splitter(split, filters):
    if split == '':
        return NoopSplitter(
            "{}:{}".format(filters.factory.nice_name, filters.value)
        )
    if split not in KNOWN_SPLITTERS:
        raise Http404("Unknown splitter key: " + split)
    return KNOWN_SPLITTERS[split]


@require_GET
def statistics(request):
    ctx = {
        'stats_kept': STATS_MAX_DAYS,
    }
    return render(request, 'statistics.html', ctx)


@require_GET
def statistics_pie(
            request,
            unit='instances', min_docs=50,
            split='', filters=''
        ):
    min_docs = int(min_docs)
    if split != '' and split not in KNOWN_FILTERS:
        raise Http404("Unexpected split: {}".format(split))

    queryset = Statistics.objects.filter(nb_documents__gte=min_docs)
    root = len(filters) <= 0 and split == ''

    filters = parse_filters(filters)
    splitter = parse_splitter(split, filters)

    queryset = filters.filter(queryset)
    if unit == 'instances':
        values = list(splitter.by_count(queryset))
        total = splitter.total_count(queryset)
    else:
        values = list(splitter.by_sum(unit, queryset))
        total = splitter.total_sum(unit, queryset)

    highscores = [
        x['nb_documents']
        for x in queryset.values("nb_documents").order_by("-nb_documents")[:5]
    ]

    current_settings = {
        'type': 'pie',
        'filters': filters,
        'unit': unit,
        'min_docs': min_docs,
        'split': split,
    }

    backlinks = []
    f = filters
    while f is not None and f.LINKABLE:
        backlinks.append({
            'type': 'pie',
            'filters': f,
            'nice_name': "{}: {}".format(
                f.factory.nice_name,
                f.value
            ),
            'unit': unit,
            'min_docs': min_docs,
            'split': '',
        })
        f = f.previous
    backlinks.reverse()

    next_filter_names = filters.get_other_filter_names()
    if splitter.field in next_filter_names:
        next_filter_names.remove(splitter.field)
    next_splits = [
        KNOWN_SPLITTERS[n]
        for n in KNOWN_SPLITTERS
        if n in next_filter_names
    ]
    next_splits.sort(key=lambda x: str(x.nice_name))

    next_filters = [
        splitter.get_filter(v[0], filters, count=v[1])
        for v in values
    ]
    next_filters.sort(key=lambda x: str(x.nice_name))

    ctx = {
        'stats_kept': STATS_MAX_DAYS,

        'settings': current_settings,
        'backlinks': backlinks,
        'root': root,
        'total': total,

        'splitter': splitter,
        'next_filters': next_filters,
        'next_splits': next_splits,

        'highscores': highscores,

        'labels': json.dumps([v[0] for v in values]),
        'datasets': json.dumps([
            {
                'data': [v[1] for v in values],
            },
        ]),
    }
    return render(request, 'statistics_pie.html', ctx)


class HistoricSimpleSplitter(object):
    def __init__(self, nice_name, table, field):
        self.nice_name = nice_name
        self.table = table
        self.field = field

    def get_values(self):
        q = self.table.objects.all().order_by("creation_date")
        for r in q:
            yield (r.creation_date, {
                str(self.nice_name): getattr(r, self.field)
            })


class HistoricGenericSplitter(object):
    def __init__(self, nice_name, table, field):
        self.nice_name = nice_name
        self.table = table
        self.field = field

    def get_values(self):
        q = self.table.objects.all().order_by(
            "history__creation_date", self.field
        )
        current_day = None
        values = {}
        for r in q:
            key = getattr(r, self.field)
            # Fixes OS
            if key == 'nt':
                key = 'Windows'
            elif 'posix' in key:
                key = 'Linux'
            elif "-" in key:
                # Fix old Paperwork versions
                key = key.split("-", 1)[0]

            if current_day is None:
                current_day = r.history.creation_date
                values = {}
            elif current_day != r.history.creation_date:
                values[str(_('Total'))] = sum(values.values())
                yield (current_day, values)
                values = {}
                current_day = r.history.creation_date
                values[key] = r.count
            elif key in values:
                values[key] += r.count
            else:
                values[key] = r.count
        if len(values) > 0:
            values[str(_('Total'))] = sum(values.values())
            yield (current_day, values)


KNOWN_HISTORIC_SPLITTERS = {
    '': HistoricSimpleSplitter(
        _("instances"), StatsHistory, 'nb_instances'),
    'docs': HistoricSimpleSplitter(
        _("documents"), StatsHistory, 'total_docs'
    ),
    'os': HistoricGenericSplitter(
        _("OS"), StatsHistoryOs, 'os_name'),
    'version': HistoricGenericSplitter(
        _("Paperwork version"), StatsHistoryVersion, 'paperwork_version'
    ),
}


@require_GET
@cache_page(24 * 60 * 60)
def statistics_historic(request, split=''):
    splitter = KNOWN_HISTORIC_SPLITTERS[split]

    values = {k: v for (k, v) in splitter.get_values()}
    keys = set()
    for vals in values.values():
        for k in vals.keys():
            if k.strip() == '':
                continue
            keys.add(k)
    keys = list(keys)
    keys.sort()

    labels = []
    datasets = {
        k: {
            'label': k,
            'data': [],
        }
        for k in keys
    }

    last_date = None
    for (date, vals) in values.items():
        if last_date is not None and date.month == last_date.month:
            continue
        last_date = date
        labels.append(date)
        for k in keys:
            v = vals.get(k, 0)
            datasets[k]['data'].append(v)

    ctx = {
        'labels': labels,
        'datasets': json.dumps(list(datasets.values())),
        'settings': {
            'type': 'historic',
            'split': split,
        }
    }
    return render(request, 'statistics_historic.html', ctx)


@require_GET
def raw_statistics(request):
    out = serializers.serialize('json', Statistics.objects.all())
    return HttpResponse(out, content_type='application/json')


def _generate_secret():
    alphabet = string.ascii_letters + string.digits
    return ''.join(secrets.choice(alphabet) for i in range(32))


@csrf_exempt
@require_POST
def bug_report_create(request):
    secret = _generate_secret()
    report = BugReport(
        secret_key=secret,
        app_name=STRICT_FILTER.sub("", request.POST['app_name']),
        app_version=STRICT_FILTER.sub("", request.POST['app_version']),
    )
    report.save()
    url = "https://openpaper.work" + reverse(
        'beacon:bug_report_show',
        kwargs={
            'report_id': report.id,
            'secret_key': report.secret_key,
        }
    )
    mail_admins(
        "New bug report for {} {}".format(report.app_name, report.app_version),
        "New bug report is available:\n" + url,
        fail_silently=True
    )
    return JsonResponse({
        "id": report.id,
        "secret": report.secret_key,
        "url": url,
    })


@csrf_exempt
@require_POST
def bug_report_add_attachment(request):
    report_id = int(request.POST['id'])
    secret_key = STRICT_FILTER.sub("", request.POST['secret'])
    file_name = STRICT_FILTER.sub("", request.POST['file_name'])
    binary = base64.decodebytes(request.POST['binary'].encode("utf-8"))

    report = BugReport.objects.get(id=report_id, secret_key=secret_key)
    attachment = BugReportAttachment(file_name=file_name, bug_report=report)
    attachment.save()
    attachment.body.save(
        "{}_{}".format(report.id, attachment.id), ContentFile(binary)
    )
    attachment.save()

    return JsonResponse({"reply": "ok"})


@staff_member_required
@require_GET
def bug_report_index(request):
    ITEMS_PER_PAGE = 50
    page_nb = int(request.GET.get("page", 0))
    nb_total_reports = BugReport.objects.count()
    reports = BugReport.objects.all().order_by("-creation_date")[
        (page_nb * ITEMS_PER_PAGE):((page_nb + 1) * ITEMS_PER_PAGE)
    ]
    ctx = {
        'reports': reports,
        'page_nb': page_nb,
        'pages': range(0, int((nb_total_reports / ITEMS_PER_PAGE) + 1)),
    }
    return render(request, 'bug_report_index.html', ctx)


@require_GET
def bug_report_show(request, report_id, secret_key):
    report_id = int(report_id)
    try:
        report = BugReport.objects.get(id=report_id, secret_key=secret_key)
    except BugReport.DoesNotExist:
        LOGGER.warning(
            "Report not found: {} - {}".format(report_id, secret_key)
        )
        raise Http404("Not found: {} - {}".format(report_id, secret_key))
    if request.user.is_authenticated and request.user.is_staff:
        if not report.read:
            report.read = True
            report.save()

    ctx = {
        'report': report,
        'attachments': BugReportAttachment.objects.filter(
            bug_report=report
        ).order_by("-file_name"),
    }
    return render(request, 'bug_report_show.html', ctx)


@require_GET
def bug_report_download_attachment(
        request, report_id, secret_key, attachment_id):
    report_id = int(report_id)
    attachment_id = int(attachment_id)
    try:
        report = BugReport.objects.get(id=report_id, secret_key=secret_key)
    except BugReport.DoesNotExist:
        LOGGER.warning(
            "Report not found: {} - {}".format(report_id, secret_key)
        )
        raise Http404("Not found: {} - {}".format(report_id, secret_key))
    try:
        attachment = BugReportAttachment.objects.get(id=attachment_id)
    except BugReport.DoesNotExist:
        LOGGER.warning(
            "Attachment not found: {}".format(attachment_id)
        )
        raise Http404("Attachment not found: {}".format(attachment_id))

    if attachment.bug_report != report:
        LOGGER.warning("Attachment and report don't match")
        raise Http404("Attachment and report don't match")

    file_name = attachment.file_name.split('/')[-1]
    response = HttpResponse(
        attachment.body,
        content_type=openpaper.backend.util.guess_mime(file_name)
    )
    # response['Content-Disposition'] = 'attachment; filename={}'.format(
    #     file_name
    # )
    return response


@staff_member_required
@require_GET
def bug_cluster_all(request):
    ITEMS_PER_PAGE = 50
    page_nb = int(request.GET.get("page", 0))
    nb_total_clusters = BugCluster.objects.count()
    clusters = BugCluster.objects.order_by(
        "solved",
        # "read",
        "-latest_paperwork_version",
        "-nb_tracebacks",
        "-latest_report",
        "id",
    )[
        (page_nb * ITEMS_PER_PAGE):((page_nb + 1) * ITEMS_PER_PAGE)
    ]
    try:
        last_traceback = BugCluster.objects.order_by("-creation_date")[0]
        generation_date = last_traceback.creation_date
    except IndexError:
        generation_date = "never"
    return render(request, 'bug_cluster_all.html', {
        "clusters": clusters,
        "nb_clusters": nb_total_clusters,
        "generation_date": generation_date,
        'pages': range(0, int((nb_total_clusters / ITEMS_PER_PAGE) + 1)),
        'page_nb': page_nb,
    })


@staff_member_required
@require_GET
def bug_cluster_view(request, cluster_id, traceback_id=None):
    if traceback_id is not None:
        traceback = Traceback.objects.get(id=int(traceback_id))
        cluster = traceback.cluster
    else:
        cluster = BugCluster.objects.get(id=int(cluster_id))
        traceback = cluster.tracebacks[0]
        cluster.read = True
        cluster.save()
    return render(request, 'bug_cluster_view.html', {
        "cluster": cluster,
        "traceback": traceback,
    })


@staff_member_required
@require_POST
def bug_cluster_mark_unread(request, cluster_id):
    cluster_id = int(cluster_id)
    cluster = BugCluster.objects.get(id=cluster_id)
    cluster.read = False
    cluster.save()
    return HttpResponseRedirect(
        redirect_to=reverse("beacon:bug_cluster_all"),
    )


@staff_member_required
@require_POST
def bug_cluster_toggle_solved(request, cluster_id):
    cluster_id = int(cluster_id)
    cluster = BugCluster.objects.get(id=cluster_id)
    cluster.solved = not cluster.solved
    cluster.save()
    return HttpResponseRedirect(
        redirect_to=reverse("beacon:bug_cluster_view", args=[cluster_id]),
    )


@staff_member_required
@require_POST
def bug_cluster_recompute(request):
    LOGGER.info("Requesting recomputing of bug clusters")
    openpaper.backend.msgqueue.Task('recompute_bug_clusters', {}).send()
    return HttpResponseRedirect(
        redirect_to=reverse("beacon:bug_cluster_all"),
    )


@staff_member_required
@require_GET
def bug_cluster_graph(request, graph_name: str):
    if ".." in graph_name or "/" in graph_name:
        return Http404("No such graph")
    with open("/var/www/uploads/bug_clusters_" + graph_name + ".png", "rb") as fd:
        content = fd.read()
    return HttpResponse(
        content,
        content_type="image/png",
    )
