import logging

from django.db import models
from django.db.models.signals import pre_delete
from django.dispatch import receiver


LOGGER = logging.getLogger(__name__)


class Statistics(models.Model):
    uuid = models.BigIntegerField(unique=True, db_index=True)
    creation_date = models.DateTimeField(auto_now_add=True)
    last_mod_date = models.DateTimeField(auto_now=True, db_index=True)
    paperwork_version = models.CharField(
        max_length=32, null=False, blank=False, db_index=True
    )

    nb_documents = models.IntegerField(blank=False, null=False, db_index=True)
    os_name = models.CharField(
        max_length=16, null=False, blank=False, db_index=True
    )
    platform_architecture = models.CharField(
        max_length=64, null=False, blank=False, db_index=True
    )
    platform_processor = models.CharField(
        max_length=16, null=False, blank=False, db_index=True
    )
    platform_distribution = models.CharField(
        max_length=64, null=True, blank=True, default="", db_index=True
    )
    country = models.CharField(
        max_length=3, null=True, blank=True, default=None, db_index=True
    )

    def __str__(self):
        return "{} ({} | {} docs | {})".format(
            self.uuid, self.paperwork_version, self.nb_documents,
            self.os_name
        )


class StatsHistory(models.Model):
    creation_date = models.DateTimeField(auto_now_add=True, db_index=True)
    nb_instances = models.IntegerField(blank=False, null=False)
    max_docs = models.IntegerField(blank=False, null=False)
    total_docs = models.IntegerField(blank=False, null=False)

    def __str__(self):
        return str(self.creation_date)


class StatsHistoryVersion(models.Model):
    history = models.ForeignKey(
        StatsHistory, on_delete=models.CASCADE, db_index=True
    )
    paperwork_version = models.CharField(
        max_length=32, null=False, blank=False, db_index=True
    )
    count = models.IntegerField(blank=False, null=False)

    def __str__(self):
        return "{}: {}".format(
            self.history.creation_date, self.paperwork_version
        )


class StatsHistoryOs(models.Model):
    history = models.ForeignKey(
        StatsHistory, on_delete=models.CASCADE, db_index=True
    )
    os_name = models.CharField(
        max_length=16, null=False, blank=False, db_index=True
    )
    count = models.IntegerField(blank=False, null=False)

    def __str__(self):
        return "{}: {}".format(self.history.creation_date, self.os_name)


class BugReport(models.Model):
    creation_date = models.DateTimeField(auto_now_add=True, db_index=True)
    update_date = models.DateTimeField(auto_now=True)
    secret_key = models.CharField(
        max_length=64, null=False, blank=False, db_index=False
    )
    app_name = models.CharField(
        max_length=32, null=False, blank=False, db_index=True
    )
    app_version = models.CharField(
        max_length=32, null=False, blank=False, db_index=True
    )
    read = models.BooleanField(default=False)

    def __str__(self):
        return "{} - {} {}".format(
            self.creation_date, self.app_name, self.app_version
        )


class BugReportAttachment(models.Model):
    creation_date = models.DateTimeField(auto_now_add=True, db_index=True)
    file_name = models.CharField(
        max_length=128, null=False, blank=False, db_index=False
    )
    body = models.FileField(upload_to='bug_report_attachments')
    bug_report = models.ForeignKey(
        BugReport, on_delete=models.CASCADE, db_index=True
    )

    def __str__(self):
        return "{} - {}".format(self.bug_report, self.file_name)


@receiver(
    pre_delete, sender=BugReportAttachment,
    dispatch_uid='bugreportattachment_delete_signal'
)
def log_deleted_question(sender, instance, using, **kwargs):
    LOGGER.info("Deleting file {} from attachment {}".format(
        instance.body.name, instance
    ))
    instance.body.delete(save=False)


class BugCluster(models.Model):
    creation_date = models.DateTimeField(auto_now_add=True)
    read = models.BooleanField(null=False)
    solved = models.BooleanField(null=False)
    latest_paperwork_version = models.CharField(max_length=64, null=False, db_index=True)
    latest_report = models.ForeignKey(BugReport, on_delete=models.CASCADE)
    nb_tracebacks = models.IntegerField()

    @property
    def tracebacks(self):
        r = Traceback.objects.filter(cluster=self).order_by("id")
        LOGGER.info(
            "%d tracebacks found for cluster %d",
            len(r), self.id
        )
        return r


class Traceback(models.Model):
    creation_date = models.DateTimeField(auto_now_add=True)
    log_file = models.ForeignKey(
        BugReportAttachment,
        on_delete=models.CASCADE,
        db_index=True,
    )
    line_start = models.IntegerField(null=False)
    line_end = models.IntegerField(null=False)
    cluster = models.ForeignKey(
        BugCluster,
        on_delete=models.CASCADE,
        db_index=True,
    )

    @property
    def content(self):
        try:
            LOGGER.info("Reading %s", self.log_file.body.path)
            with open(self.log_file.body.path, "r", encoding="utf-8") as fd:
                lines = fd.readlines()
            lines = lines[self.line_start:self.line_end]
            lines = [line.rstrip() for line in lines]
            return "\n".join(lines)
        except Exception as exc:
            LOGGER.error("Exception while reading %s: %s", self.log_file.body.path, repr(exc))
            return ""
