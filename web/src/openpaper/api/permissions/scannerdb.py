import logging

import rest_framework
import rest_framework.permissions

from . import AbstractHasKeyOrReadOnly


LOGGER = logging.getLogger(__name__)


class _AbstractHasKeyOrReadOnly(AbstractHasKeyOrReadOnly):
    expected_key_name_prefix = "scannerdb-"
    expected_admin_key_name_prefix = "scannerdb-admin-"

    def has_admin_key(perm, request):
        # Allow admin keys to make any modification to the report,
        # even unseal them.
        key = perm.get_key(request)
        if key:
            # we got the key string, but we need the model to get the name
            # we actually gave it
            key = perm.model.objects.get_from_key(key)
            if key.name.startswith(perm.expected_admin_key_name_prefix):
                return True
        return False


class AbstractScannerDbPermission(_AbstractHasKeyOrReadOnly):
    def has_permission(self, request, view):
        if self.has_admin_key(request):
            return True

        if request.method not in rest_framework.permissions.SAFE_METHODS:
            data = request.data
            if ('moderated' in data and
                    data['moderated'] is not False and
                    data['moderated'] != "False"):
                LOGGER.warning(
                    "AbstractScannerDbPermission.has_permission():"
                    " %s %s: False",
                    request, view
                )
                return False
        return super().has_permission(request, view)

    def has_object_permission(self, request, view, obj):
        if self.has_admin_key(request):
            return True
        return super().has_object_permission(request, view, obj)


class ScannerDbHasKeyOrReadOnly(AbstractScannerDbPermission):
    def has_object_permission(self, request, view, obj):
        if self.has_admin_key(request):
            return True

        if request.method not in rest_framework.permissions.SAFE_METHODS:
            if request.method != 'POST':
                # we only allow adding data, not modifying them
                LOGGER.warning(
                    "ScannerDbHasKeyOrReadOnly.has_object_permission():"
                    " %s %s %s: method != POST -> False",
                    request, view, obj
                )
                return False
            if obj.moderated:
                LOGGER.warning(
                    "ScannerDbHasKeyOrReadOnly.has_object_permission():"
                    " %s %s %s: obj.moderated -> False",
                    request, view, obj
                )
                return False
        return super().has_object_permission(request, view, obj)


class ScannerDbReportPermission(AbstractScannerDbPermission):
    def has_object_permission(self, request, view, obj):
        if self.has_admin_key(request):
            return True

        # Allow admin keys to make any modification to the report,
        # even unseal them.
        key = self.get_key(request)
        if key:
            # we got the key string, but we need the model to get the name
            # we actually gave it
            key = self.model.objects.get_from_key(key)
            if key.name.startswith(self.expected_admin_key_name_prefix):
                return True

        if request.method not in rest_framework.permissions.SAFE_METHODS:
            # as long as it's not sealed, we allow updates
            if obj.moderated or obj.sealed:
                LOGGER.warning(
                    "ScannerDbReportPermission.has_object_permission():"
                    " %s %s %s: obj.moderated(%s) / obj.sealed(%s) -> False",
                    request, view, obj, obj.moderated, obj.sealed
                )
                return False
            if request.data.get('secret_key', "") != obj.secret_key:
                LOGGER.warning(
                    "ScannerDbReportPermission.has_object_permission():"
                    " %s %s %s: report secret key mismatch -> False",
                    request, view, obj
                )
                return False
        return super().has_object_permission(request, view, obj)


class ScannerDbReportAttachmentPermission(_AbstractHasKeyOrReadOnly):
    def has_permission(self, request, view):
        if self.has_admin_key(request):
            return True
        return super().has_permission(request, view)

    def has_object_permission(self, request, view, obj):
        if self.has_admin_key(request):
            return True
        if request.method not in rest_framework.permissions.SAFE_METHODS:
            # as long as it's not sealed, we allow updates
            if obj.report.moderated or obj.report.sealed:
                LOGGER.warning(
                    "ScannerDbReportAttachmentPermission"
                    ".has_object_permission():"
                    " %s %s %s:"
                    " obj.report.moderated(%s) / obj.report.sealed(%s)"
                    " -> False",
                    request, view, obj, obj.report.moderated, obj.report.sealed
                )
                return False
            if request.data.get('secret_key', "") != obj.report.secret_key:
                LOGGER.warning(
                    "ScannerDbReportAttachmentPermission"
                    ".has_object_permission():"
                    " %s %s %s: report secret key mismatch -> False",
                    request, view, obj
                )
                return False
        return super().has_object_permission(request, view, obj)
