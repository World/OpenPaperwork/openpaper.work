import logging
import os
import pickle
import time

import pika

from django.core.wsgi import get_wsgi_application


LOGGER = logging.getLogger(__name__)


os.environ["DJANGO_SETTINGS_MODULE"] = "openpaper.settings"
# WORKAROUND: we need to call get_wsgi_application() in order to be able to
# import the models
get_wsgi_application()


import worker.bugs  # noqa E402
import worker.ocr  # noqa E402


JOB_RUNNERS = {
    "ocr": worker.ocr.Ocr(),
    "recompute_bug_clusters": worker.bugs.BugClustersComputation(),
}


def rabbitmq_callback(channel, method, properties, body):
    body = pickle.loads(body)
    LOGGER.info(f"Received {body}")
    JOB_RUNNERS[body['job']].do(**body['args'])
    channel.basic_ack(delivery_tag=method.delivery_tag)


def main():
    logging.basicConfig(
        level=logging.INFO,
        format="[%(asctime)s] [%(levelname)-7s] [%(module)s] [%(funcName)s()] %(message)s",
        handlers=[
            logging.StreamHandler()
        ],
        force=True,
    )

    LOGGER.info("Connecting to broker ...")
    connection = None
    while connection is None:
        try:
            connection = pika.BlockingConnection(
                pika.ConnectionParameters(host=os.environ['RABBITMQ_HOST'])
            )
        except Exception as exc:
            LOGGER.info(f"[ERROR] Failed to connect to broker: {repr(exc)}")
            time.sleep(5)
    channel = connection.channel()
    channel.queue_declare(queue='tasks')
    channel.basic_consume(
        queue='tasks',
        on_message_callback=rabbitmq_callback,
        auto_ack=False
    )
    LOGGER.info("READY")
    channel.start_consuming()


if __name__ == "__main__":
    main()
