#!/usr/bin/env python3

"""
Upload an attachment on a report. Requires an admin API key.
"""

import os
import os.path
import requests
import sys


def _check_status_code_200(r):
    if r.status_code - (r.status_code % 100) != 200:
        raise Exception("Unexpected HTTP reply: {} - {}\n{}".format(
            r.status_code, r.reason, r.text
        ))


def main():
    base_url = sys.argv[1] if len(sys.argv) > 1 else None
    report_url = sys.argv[2] if len(sys.argv) > 2 else None
    api_key = sys.argv[3] if len(sys.argv) > 3 else None
    attachment_filepaths = sys.argv[4:]

    if api_key is None:
        print("Syntax: ")
        print(
            "  {} <base_url> <report url> <API key> <attachment file>"
            " [<file> [<file> ...]]".format(
                sys.argv[0]
            )
        )
        return

    headers = {"Authorization": "Api-Key " + api_key}

    print("GET {} ...".format(base_url))
    r = requests.get(base_url)
    _check_status_code_200(r)
    routes = r.json()

    print("GET {} ...".format(report_url))
    r = requests.get(report_url, headers=headers)
    _check_status_code_200(r)
    report = r.json()

    print("PATCH {} (sealed=False;moderated=False) ...".format(report_url))
    r = requests.patch(
        report_url,
        headers=headers,
        data={
            'sealed': False,
            'moderated': False,
        },
        allow_redirects=False
    )
    _check_status_code_200(r)
    try:
        for attachment_filepath in attachment_filepaths:
            with open(attachment_filepath, 'rb') as content:
                # upload the attachment
                data = {
                    "report": report_url,
                    "file_name": os.path.basename(attachment_filepath),
                }
                files = {
                    "attachment": content,
                }
                print("POST {} ...".format(routes['scan_report_attachments']))
                r = requests.post(
                    routes['scan_report_attachments'],
                    headers=headers,
                    data=data,
                    files=files,
                    allow_redirects=False
                )
                _check_status_code_200(r)
    finally:
        print("PATCH {} (sealed=True;moderated={}) ...".format(
            report_url, report['moderated']
        ))
        r = requests.patch(
            report_url,
            headers=headers,
            data={
                'sealed': True,
                'moderated': report['moderated'],
            },
            allow_redirects=False
        )
        _check_status_code_200(r)


if __name__ == "__main__":
    main()
