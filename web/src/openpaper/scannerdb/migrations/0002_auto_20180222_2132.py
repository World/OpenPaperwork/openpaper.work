# -*- coding: utf-8 -*-
# Generated by Django 1.11.9 on 2018-02-22 21:32
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('scannerdb', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='report',
            name='body',
            field=models.TextField(editable=False),
        ),
    ]
