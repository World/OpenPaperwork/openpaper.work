from django import template
from django.urls import NoReverseMatch
from django.urls import resolve
from django.urls import reverse
from django.utils import translation


register = template.Library()


class HasTranslatedURL(template.Node):
    def __init__(self):
        pass

    def render(self, context):
        view = resolve(context['request'].path)
        try:
            url_name = view.namespace + ":" + view.url_name
            reverse(url_name, args=view.args, kwargs=view.kwargs)
        except NoReverseMatch:
            context['has_translated_url'] = False
        context['has_translated_url'] = True
        return ""


@register.tag(name='check_has_translated_url')
def do_has_translated_url(parser, token):
    return HasTranslatedURL()


class TranslatedURL(template.Node):
    def __init__(self, language):
        self.language = language

    def render(self, context):
        view = resolve(context['request'].path)
        request_language = translation.get_language()
        translation.activate(self.language)
        try:
            url_name = view.namespace + ":" + view.url_name
            url = reverse(url_name, args=view.args, kwargs=view.kwargs)
        except NoReverseMatch:
            url = reverse("home:index")
        finally:
            translation.activate(request_language)
        return url


@register.tag(name='translate_url')
def do_translate_url(parser, token):
    language = token.split_contents()[1]
    return TranslatedURL(language)
