install:
	make -C web install
	make -C worker install


check:
	make -C web check
	make -C worker check


test:
	make -C web test
	make -C worker test


PHONY: \
	check \
	test
