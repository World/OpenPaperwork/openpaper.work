import os
import sys

DEBUG = bool(int(os.getenv("DJANGO_DEBUG", "0")))
TEMPLATE_OPTIONS = {'debug': DEBUG}
TEMPLATE_DEBUG = DEBUG

# Both testing and prod are behind a reverse proxy
SECURE_PROXY_SSL_HEADER = ('HTTP_X_FORWARDED_PROTO', 'https')

SECRET_KEY = os.getenv("DJANGO_SECRET", "bcdef")

ADMINS = [
    ('Jflesch', 'jflesch@openpaper.work'),
]

MEDIA_ROOT = '/var/www/uploads'

ALLOWED_HOSTS = [
    'localhost',
    '127.0.0.1',
    '[::1]',
    'openpaper.work',
    'testing.openpaper.work',
    'www.openpaper.work',
    'web',
]

NO_REPLY_EMAIL = 'no-reply@openpaper.work'
DEFAULT_FROM_EMAIL = 'no-reply@openpaper.work'
SERVER_EMAIL = 'no-reply@openpaper.work'

EMAIL_HOST = os.getenv("DJANGO_EMAIL_SERVER", None)
if EMAIL_HOST is None:
    sys.stderr.write("#### EMAIL DISABLED #####\n")
    sys.stderr.flush()
    EMAIL_BACKEND = 'django.core.mail.backends.dummy.EmailBackend'
else:
    EMAIL_PORT = int(os.getenv("DJANGO_EMAIL_PORT", 587))
    # EMAIL_HOST_USER = os.getenv("DJANGO_EMAIL_USER", None)
    # EMAIL_HOST_PASSWORD = os.getenv("DJANGO_EMAIL_PASSWORD", None)
    EMAIL_USE_TLS = False
    sys.stderr.write("#### EMAIL ENABLED: {} {} #####\n".format(
        EMAIL_HOST, EMAIL_PORT
    ))
    sys.stderr.flush()

if 'MYSQL_DATABASE' in os.environ:
    sys.stderr.write(
        "###### USING MYSQL: {} #####\n".format(os.environ['MYSQL_DATABASE'])
    )
    sys.stderr.flush()
    DATABASES = {
        'default': {
            'ENGINE': 'django.db.backends.mysql',
            'NAME': os.environ["MYSQL_DATABASE"],
            'USER': os.environ["MYSQL_USER"],
            'PASSWORD': os.environ["MYSQL_PASSWORD"],
            'HOST': 'db',
            'PORT': '3306',
            'OPTIONS': {'init_command': "SET sql_mode='STRICT_TRANS_TABLES'"},
        },
    }
else:
    sys.stderr.write("###### USING SQLITE #####\n")
    sys.stderr.flush()
    DATABASES = {
        'default': {
            'ENGINE': 'django.db.backends.sqlite3',
            'NAME': 'development.sqlite3',
            'USER': '',
            'PASSWORD': '',
            'HOST': '',
            'PORT': '',
        },
    }

LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'handlers': {
        'console': {
            'level': 'INFO',
            'class': 'logging.StreamHandler',
        },
    },
    'loggers': {
        '': {  # any
            'handlers': ['console'],
            'level': 'INFO',
            'propagate': True,
        },
    },
}

# CACHES = {
#     'default': {
#         'BACKEND': 'django.core.cache.backends.dummy.DummyCache',
#     }
# }
