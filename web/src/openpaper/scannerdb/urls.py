# -*- coding: utf-8 -*-
from django.urls import re_path

from . import views


urlpatterns = [
    re_path(r'^$', views.home, name='home'),
    re_path(r'^report/(?P<report_id>[0-9]+)/$', views.report, name='report'),
    re_path(
        r'^scanner/(?P<scanner_id>[0-9]+)/$',
        views.scanner, name='scanner'
    ),
    re_path(
        r'^vendor/(?P<manufacturer_id>[0-9]+)/$',
        views.manufacturer, name='vendor'
    ),
    re_path(
        r'^report/(?P<report_id>[0-9]+)/delete$',
        views.delete_report, name='delete_report'
    ),
    re_path(
        r'^report/(?P<report_id>[0-9]+)/comment$',
        views.comment_report, name='comment_report'
    ),
    re_path(
        r'^report/(?P<report_id>[0-9]+)/traces_colorized/'
        '(?P<attachment_id>[0-9]+)/?',
        views.report_traces_colorized, name='report_traces_colorized'
    ),
    re_path(
        r'^report/(?P<report_id>[0-9]+)/validate$',
        views.validate_report, name='validate_report'
    ),
    re_path(
        r'^report/(?P<report_id>[0-9]+)/analyze$',
        views.analyze_report, name='analyze_report'
    ),
    re_path(
        r'^analyze_all$',
        views.analyze_all, name='analyze_all'
    ),
]
