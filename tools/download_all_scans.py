#!/usr/bin/env python3

"""
Download all valid scans from all reports.
Useful to figure out what could be valid material for IA / Computer Vision.
Also useful to find quickly reports that were marked successful when they
actually were not.
"""

import os
import sys

import requests


def main():
    out_dir = sys.argv[1] if len(sys.argv) > 1 else None
    if out_dir is None:
        print("Syntax:")
        print("  {} <output directory> [<api_url>]".format(sys.argv[0]))
        return

    base_url = (
        sys.argv[2]
        if len(sys.argv) > 2
        else "https://openpaper.work/api/v1/scannerdb/"
    )

    print("GET {} ...".format(base_url))
    routes = requests.get(base_url)
    routes = routes.json()

    os.makedirs(out_dir, mode=0o700, exist_ok=True)

    progression = 0

    attachments_url = routes['scan_report_attachments']
    while attachments_url is not None:
        print("")
        print("GET {} ...".format(attachments_url))
        attachments = requests.get(attachments_url)
        attachments = attachments.json()

        total = attachments['count']

        for attachment in attachments['results']:
            progression += 1

            if not attachment['mime_type'].startswith("image/"):
                continue
            if not attachment['file_name'].startswith("image_"):
                continue

            print("")
            print("GET {} ...".format(attachment['report']))
            report = requests.get(attachment['report'])
            report = report.json()
            report_attrs = {
                'moderated': None,
                'sealed': None,
                'successful': None,
            }
            for k in list(report_attrs.keys()):
                report_attrs[k] = report[k] if k in report else False

            print("Report: {}".format(report_attrs))
            if False in report_attrs.values():
                print("Skipping")
                continue

            resolution = report.get(
                'data', {}
            ).get(
                'scantest', {}
            ).get(
                'config', {}
            ).get('resolution', None)
            if resolution is None:
                print("Resolution is misisng. Skipping")
                continue
            if resolution < 75:
                print("Resolution too low: {}dpi. Skipping".format(resolution))
                continue

            file_name = "r{:04}_s{:04}_{}.png".format(
                report['id'], attachment['id'],
                attachment['file_name'].replace(" ", "_").rsplit(".", 1)[0]
            )

            print("GET {} ==> {} ...".format(
                attachment['attachment'], file_name
            ))
            r = requests.get(attachment['attachment'])
            out_path = os.path.join(out_dir, file_name)
            with open(out_path, 'wb') as fd:
                fd.write(r.content)

            print("")
            print("Progression: {}%".format(
                int(progression * 100 / total)
            ))

        attachments_url = attachments['next']


if __name__ == "__main__":
    main()
