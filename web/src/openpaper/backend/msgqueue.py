import pickle
import logging

import django.conf

import pika


LOGGER = logging.getLogger(__name__)


class _PikaConnection:
    def __init__(self):
        self.connection = None
        self.channel = None

    def __enter__(self):
        LOGGER.info("Connecting to msg queue broker")
        self.connection = pika.BlockingConnection(
            pika.ConnectionParameters(host=django.conf.settings.RABBITMQ_HOST)
        )
        self.channel = self.connection.channel()
        return self.channel

    def __exit__(self, *args):
        LOGGER.info("Disconnecting from msg queue broker")
        self.connection.close()


class Task:
    def __init__(self, job_type, args: dict):
        self.job_type = job_type
        self.args = args

    def send(self):
        with _PikaConnection() as channel:
            channel.queue_declare(queue='tasks')
            channel.basic_publish(
                exchange='', routing_key='tasks', body=pickle.dumps({
                    'job': self.job_type,
                    'args': self.args
                })
            )
