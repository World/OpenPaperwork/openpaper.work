# Generated by Django 2.2.12 on 2020-04-11 18:44

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('beacon', '0004_remove_statistics_cpu_count'),
    ]

    operations = [
        migrations.CreateModel(
            name='BugReport',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('creation_date', models.DateTimeField(auto_now_add=True, db_index=True)),
                ('update_date', models.DateTimeField(auto_now=True)),
                ('secret_key', models.CharField(max_length=64)),
                ('app_name', models.CharField(db_index=True, max_length=32)),
                ('app_version', models.CharField(db_index=True, max_length=32)),
            ],
        ),
        migrations.CreateModel(
            name='BugReportAttachment',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('creation_date', models.DateTimeField(auto_now_add=True, db_index=True)),
                ('file_name', models.CharField(max_length=64)),
                ('body', models.FileField(upload_to='bug_report_attachments')),
                ('bug_report', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='beacon.BugReport')),
            ],
        ),
    ]
