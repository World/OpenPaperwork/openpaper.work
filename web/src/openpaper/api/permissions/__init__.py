import logging

import rest_framework
import rest_framework.permissions
import rest_framework_api_key
import rest_framework_api_key.permissions


LOGGER = logging.getLogger(__name__)


class DenyAll(rest_framework.permissions.BasePermission):
    message = "Denied"

    def has_permission(self, request, view):
        LOGGER.warning("DenyAll.has_permission(): %s %s", request, view)
        return False

    def has_object_permission(self, request, view, obj):
        LOGGER.warning(
            "DenyAll.has_object_permission(): %s %s %s",
            request, view, obj
        )
        return False


class ReadOnly(rest_framework.permissions.BasePermission):
    message = "Always read only"

    def has_permission(self, request, view):
        r = request.method in rest_framework.permissions.SAFE_METHODS
        if not r:
            LOGGER.warning(
                "ReadOnly.has_permission(): %s %s: False",
                request, view
            )
        return r

    def has_object_permission(self, request, view, obj):
        r = request.method in rest_framework.permissions.SAFE_METHODS
        if not r:
            LOGGER.warning(
                "ReadOnly.has_object_permission(): %s %s %s: False",
                request, view, obj
            )
        return r


class AbstractHasKeyOrReadOnly(
        rest_framework_api_key.permissions.HasAPIKey):
    message = "Read only"
    expected_key_name_prefix = None

    def _check(self, request, cb, *args):
        if request.method in rest_framework.permissions.SAFE_METHODS:
            return True
        key = self.get_key(request)
        if not key:
            return False
        # we got the key string, but we need the model to get the name we
        # actually gave it
        key = self.model.objects.get_from_key(key)
        if not key.name.startswith(self.expected_key_name_prefix):
            return False
        return cb(request, *args)

    def has_permission(self, request, view):
        r = self._check(request, super().has_permission, view)
        if not r:
            LOGGER.warning(
                "AbstractHasKeyOrReadOnly.has_permission(): %s %s: False",
                request, view
            )
        return r

    def has_object_permission(self, request, view, obj):
        r = self._check(request, super().has_object_permission, view, obj)
        if not r:
            LOGGER.warning(
                "AbstractHasKeyOrReadOnly.has_object_permission():"
                " %s %s %s: False",
                request, view, obj
            )
        return r
