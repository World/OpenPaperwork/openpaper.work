from django.apps import AppConfig


class ScannerdbConfig(AppConfig):
    name = 'openpaper.scannerdb'
