import io
import json

import django.conf
import django.test

import rest_framework.test
import rest_framework_api_key.models

import openpaper.backend.models.scannerdb


class TestScannerDbGet(django.test.TestCase):
    def setUp(self):
        m_a = openpaper.backend.models.scannerdb.ScannerManufacturer.objects.create(
            id=1,
            moderated=True,
            name="test manufacturer A",
        )
        m_b = openpaper.backend.models.scannerdb.ScannerManufacturer.objects.create(
            id=2,
            moderated=False,
            name="test manufacturer B",
        )

        s_a = openpaper.backend.models.scannerdb.Scanner.objects.create(
            id=1,
            moderated=True,
            name="Test scanner A",
            manufacturer=m_a
        )
        openpaper.backend.models.scannerdb.Scanner.objects.create(
            id=2,
            moderated=True,
            name="Test scanner B",
            manufacturer=m_a
        )
        openpaper.backend.models.scannerdb.Scanner.objects.create(
            id=3,
            moderated=False,
            name="Test scanner C",
            manufacturer=m_b
        )

        r_a = openpaper.backend.models.scannerdb.ScanReport.objects.create(
            id=1,
            sealed=True,
            scanner=s_a,
            moderated=True,
            successful=True,
            admin_comment="CAMION 1 !",
            user_email="a@b.com",
        )
        r_b = openpaper.backend.models.scannerdb.ScanReport.objects.create(
            id=2,
            sealed=True,
            scanner=s_a,
            moderated=False,
            successful=True,
            admin_comment="CAMION 2 !",
            user_email="a@b.com",
        )
        r_c = openpaper.backend.models.scannerdb.ScanReport.objects.create(
            id=3,
            sealed=True,
            scanner=s_a,
            moderated=True,
            successful=True,
            admin_comment="CAMION 3 !",
            user_email="a@b.com",
        )

        # In each report, create the following tree:
        # Report
        # |-- a (1-2)
        # |   |-- aa (1-1)
        # |   |-- ab (2-2)
        # |
        # |-- b (3-4)
        # |   |-- ba (3-3)
        # |   |-- bb (4-4)
        for report in (r_a, r_b, r_c):
            openpaper.backend.models.scannerdb.ScanReportTreeNode.objects.create(
                report=report,
                name="node_a",
                lft=1,
                rgt=2,
            )
            fs_aa = (
                openpaper.backend.models.scannerdb.ScanReportFieldString.objects.create(
                    value="value_aa"
                )
            )
            openpaper.backend.models.scannerdb.ScanReportTreeNode.objects.create(
                report=report,
                name="node_aa",
                lft=1,
                rgt=1,
                content=fs_aa
            )
            fs_ab = (
                openpaper.backend.models.scannerdb.ScanReportFieldString.objects.create(
                    value="value_ab"
                )
            )
            openpaper.backend.models.scannerdb.ScanReportTreeNode.objects.create(
                report=report,
                name="node_ab",
                lft=2,
                rgt=2,
                content=fs_ab
            )

            openpaper.backend.models.scannerdb.ScanReportTreeNode.objects.create(
                report=report,
                name="node_b",
                lft=3,
                rgt=4,
            )
            fs_ba = (
                openpaper.backend.models.scannerdb.ScanReportFieldString.objects.create(
                    value="value_ba"
                )
            )
            openpaper.backend.models.scannerdb.ScanReportTreeNode.objects.create(
                report=report,
                name="node_ba",
                lft=3,
                rgt=3,
                content=fs_ba
            )
            fs_bb = (
                openpaper.backend.models.scannerdb.ScanReportFieldString.objects.create(
                    value="value_bb"
                )
            )
            openpaper.backend.models.scannerdb.ScanReportTreeNode.objects.create(
                report=report,
                name="node_bb",
                lft=4,
                rgt=4,
                content=fs_bb
            )

    def test_get_manufacturers(self):
        request = rest_framework.test.APIClient()
        request = request.get("/api/v1/scannerdb/scanner_manufacturers/")
        self.assertEqual(request.status_code, 200)
        body = json.loads(request.content)
        self.assertEqual(body['count'], 2)
        self.assertEqual(body['results'][0]["name"], "test manufacturer A")
        self.assertEqual(
            body['results'][0]["url"],
            "http://testserver/api/v1/scannerdb/scanner_manufacturers/1/"
        )
        self.assertEqual(body['results'][1]["name"], "test manufacturer B")
        self.assertEqual(
            body['results'][1]["url"],
            "http://testserver/api/v1/scannerdb/scanner_manufacturers/2/"
        )

    def test_get_manufacturer(self):
        request = rest_framework.test.APIClient()
        request = request.get("/api/v1/scannerdb/scanner_manufacturers/1/")
        self.assertEqual(request.status_code, 200)
        body = json.loads(request.content)
        self.assertEqual(body["name"], "test manufacturer A")
        self.assertEqual(body["moderated"], True)
        self.assertEqual(body['scanners'], [
            'http://testserver/api/v1/scannerdb/scanners/1/',
            'http://testserver/api/v1/scannerdb/scanners/2/'
        ])

    def test_get_scanners(self):
        request = rest_framework.test.APIClient()
        request = request.get("/api/v1/scannerdb/scanners/")
        self.assertEqual(request.status_code, 200)
        body = json.loads(request.content)
        self.assertEqual(body['count'], 3)
        self.assertEqual(body['results'][0]['name'], "Test scanner A")
        self.assertEqual(
            body['results'][0]['url'],
            "http://testserver/api/v1/scannerdb/scanners/1/"
        )
        self.assertEqual(
            body['results'][0]['manufacturer'],
            "http://testserver/api/v1/scannerdb/scanner_manufacturers/1/"
        )
        self.assertEqual(body['results'][1]['name'], "Test scanner B")
        self.assertEqual(
            body['results'][1]['manufacturer'],
            "http://testserver/api/v1/scannerdb/scanner_manufacturers/1/"
        )
        self.assertEqual(
            body['results'][1]['url'],
            "http://testserver/api/v1/scannerdb/scanners/2/"
        )
        self.assertEqual(body['results'][2]['name'], "Test scanner C")
        self.assertEqual(
            body['results'][2]['manufacturer'],
            "http://testserver/api/v1/scannerdb/scanner_manufacturers/2/"
        )
        self.assertEqual(
            body['results'][2]['url'],
            "http://testserver/api/v1/scannerdb/scanners/3/"
        )

    def test_get_scanner(self):
        request = rest_framework.test.APIClient()
        request = request.get("/api/v1/scannerdb/scanners/2/")
        self.assertEqual(request.status_code, 200)
        body = json.loads(request.content)
        self.assertEqual(body['name'], "Test scanner B")
        self.assertEqual(body['moderated'], True)
        self.assertEqual(
            body['manufacturer'],
            'http://testserver/api/v1/scannerdb/scanner_manufacturers/1/'
        )

    def test_get_reports(self):
        request = rest_framework.test.APIClient()
        request = request.get("/api/v1/scannerdb/scan_reports/")
        self.assertEqual(request.status_code, 200)
        body = json.loads(request.content)
        self.assertEqual(body['count'], 2)  # one is not moderated yet
        self.assertEqual(body['results'][0]['sealed'], True)
        self.assertNotIn('data', body['results'][0])
        self.assertNotIn('secret_key', body['results'][0])
        self.assertNotIn('user_email', body['results'][0])

    def test_get_report(self):
        request = rest_framework.test.APIClient()
        request = request.get("/api/v1/scannerdb/scan_reports/3/")
        self.assertEqual(request.status_code, 200)
        body = json.loads(request.content)
        self.assertEqual(body['sealed'], True)
        self.assertIn('data', body)
        self.assertNotIn('secret_key', body)
        self.assertNotIn('user_email', body)
        self.assertEqual(
            body['data'], {
                'node_a': {'node_aa': 'value_aa', 'node_ab': 'value_ab'},
                'node_b': {'node_ba': 'value_ba', 'node_bb': 'value_bb'},
            }
        )


class TestScannerDbSecurity(django.test.TestCase):
    def test_no_api_key(self):
        body = {
            "moderated": False,
            "name": "test manufacturer A",
        }

        request = rest_framework.test.APIClient()
        request = request.post(
            "/api/v1/scannerdb/scanner_manufacturers/", body
        )
        self.assertEqual(request.status_code, 403)

    def test_wrong_key_prefix(self):
        body = {
            "moderated": False,
            "name": "test manufacturer A",
        }
        (api_key, key) = (
            rest_framework_api_key.models.APIKey.objects.create_key(
                name="camion-whatever"
            )
        )

        request = rest_framework.test.APIClient()
        request.credentials(HTTP_AUTHORIZATION="Api-Key {}".format(key))
        request = request.post(
            "/api/v1/scannerdb/scanner_manufacturers/", body
        )
        self.assertEqual(request.status_code, 403)

    def test_put(self):
        openpaper.backend.models.scannerdb.ScannerManufacturer.objects.create(
            id=1,
            moderated=False,
            name="test manufacturer A",
        )
        body = {
            "moderated": False,
            "name": "test manufacturer Z",
        }
        (api_key, key) = (
            rest_framework_api_key.models.APIKey.objects.create_key(
                name="scannerdb-whatever"
            )
        )

        request = rest_framework.test.APIClient()
        request.credentials(HTTP_AUTHORIZATION="Api-Key {}".format(key))
        request = request.put(
            "/api/v1/scannerdb/scanner_manufacturers/1/", body
        )
        self.assertEqual(request.status_code, 403)

    def test_moderated(self):
        body = {
            "moderated": True,  # not allowed
            "name": "test manufacturer Z",
        }
        (api_key, key) = (
            rest_framework_api_key.models.APIKey.objects.create_key(
                name="scannerdb-whatever"
            )
        )

        request = rest_framework.test.APIClient()
        request.credentials(HTTP_AUTHORIZATION="Api-Key {}".format(key))
        request = request.post(
            "/api/v1/scannerdb/scanner_manufacturers/", body
        )
        self.assertEqual(request.status_code, 403)


class TestWithAPIKey(django.test.TestCase):
    def setUp(self):
        (api_key, key) = (
            rest_framework_api_key.models.APIKey.objects.create_key(
                name="scannerdb-whatever"
            )
        )
        self.key = key


class TestScannerDbPostManufacturer(TestWithAPIKey):
    def test_post_manufacturer(self):
        self.assertEqual(
            openpaper.backend.models.scannerdb.ScannerManufacturer.objects.count(), 0
        )

        body = {
            "moderated": False,
            "name": "test manufacturer A",
        }

        request = rest_framework.test.APIClient()
        request.credentials(HTTP_AUTHORIZATION="Api-Key {}".format(self.key))
        request = request.post(
            "/api/v1/scannerdb/scanner_manufacturers/", body
        )
        self.assertEqual(request.status_code, 201)

        self.assertEqual(
            openpaper.backend.models.scannerdb.ScannerManufacturer.objects.count(), 1
        )
        m = openpaper.backend.models.scannerdb.ScannerManufacturer.objects.get()
        self.assertEqual(m.name, "test manufacturer A")
        self.assertEqual(m.moderated, False)


class TestSCannerDbPostScanner(TestWithAPIKey):
    def setUp(self):
        super().setUp()
        self.m_a = openpaper.backend.models.scannerdb.ScannerManufacturer.objects.create(
            id=1,
            moderated=True,
            name="test manufacturer A",
        )

    def test_post_scanner(self):
        self.assertEqual(
            openpaper.backend.models.scannerdb.Scanner.objects.count(), 0
        )

        body = {
            "moderated": False,
            "name": "test scanner A",
            "manufacturer": (
                "http://testserver/api/v1/scannerdb/scanner_manufacturers/1/"
            ),
        }

        request = rest_framework.test.APIClient()
        request.credentials(HTTP_AUTHORIZATION="Api-Key {}".format(self.key))
        request = request.post(
            "/api/v1/scannerdb/scanners/", body
        )
        self.assertEqual(request.status_code, 201)
        body = json.loads(request.content)
        self.assertTrue(body['url'].endswith("scanners/1/"))

        self.assertEqual(
            openpaper.backend.models.scannerdb.ScannerManufacturer.objects.count(), 1
        )


class TestScannerDbPostReport(TestWithAPIKey):
    def setUp(self):
        super().setUp()
        self.m_a = openpaper.backend.models.scannerdb.ScannerManufacturer.objects.create(
            id=22,
            moderated=True,
            name="test manufacturer A",
        )
        self.s_a = openpaper.backend.models.scannerdb.Scanner.objects.create(
            id=3,
            moderated=False,
            name="Test scanner C",
            manufacturer=self.m_a
        )

        django.conf.settings.MEDIA_ROOT = "/tmp"

    def test_create_report(self):
        self.assertEqual(
            openpaper.backend.models.scannerdb.ScanReport.objects.count(), 0
        )

        # push the report entry itself
        body = {
            "scanner": "http://testserver/api/v1/scannerdb/scanners/3/",
            "sealed": False,
            "successful": True,
            "os": "Linux",
            "scan_library": "Libinsane -1.2.3",
            "scan_program": "IronScanner -1.2.3",
            "scan_source": "Flatbed",
            "scanner_type": "Flatbed",
        }
        request = rest_framework.test.APIClient()
        request.credentials(HTTP_AUTHORIZATION="Api-Key {}".format(self.key))
        request = request.post(
            "/api/v1/scannerdb/scan_reports/", body, format="json"
        )
        self.assertEqual(request.status_code, 201)
        body = json.loads(request.content)
        self.assertIn('url', body)
        self.assertIn('secret_key', body)
        report_url = body['url']
        report_secret = body['secret_key']
        self.assertTrue(report_url.endswith("scan_reports/1/"))

        # push report body
        body = {
            "secret_key": report_secret,
            "data": {
                "node_a": {"node_aa": "value_aa", "node_ab": "value_ab"},
                "node_b": {"node_ba": "value_ba", "node_ab": "value_bb"},
            }
        }
        request = rest_framework.test.APIClient()
        request.credentials(HTTP_AUTHORIZATION="Api-Key {}".format(self.key))
        request = request.post(
            report_url + "data", body, format="json"
        )
        self.assertEqual(request.status_code, 201)

        r = openpaper.backend.models.scannerdb.ScanReport.objects.get()
        tree = r.get_tree(None)

        self.assertEqual(tree, {
            "node_a": {"node_aa": "value_aa", "node_ab": "value_ab"},
            "node_b": {"node_ba": "value_ba", "node_ab": "value_bb"},
        })

        # push a text file
        file_content = io.BytesIO(b"abcdef")
        body = {
            "report": report_url,
            "file_name": "toto.txt",
            "attachment": file_content,
            "secret_key": report_secret,
        }
        request = rest_framework.test.APIClient()
        request.credentials(HTTP_AUTHORIZATION="Api-Key {}".format(self.key))
        request = request.post(
            "/api/v1/scannerdb/scan_report_attachments/", body
        )
        self.assertEqual(request.status_code, 201)
        body = json.loads(request.content)
        self.assertIn('url', body)

        objs = openpaper.backend.models.scannerdb.ScanReportAttachment.objects.all()
        self.assertEqual(len(objs), 1)
        attachment = objs[0]
        self.assertIsNotNone(attachment.attachment)
        # Make sure the attachment has been saved
        with attachment.attachment.open() as fd:
            fd.read()

        # seal the report
        body = {
            "scanner": "http://testserver/api/v1/scannerdb/scanners/3/",
            "successful": True,
            "sealed": True,
            "secret_key": report_secret,
            "os": "Linux",
            "scan_library": "Libinsane -1.2.3",
            "scan_program": "IronScanner -1.2.3",
            "scan_source": "Flatbed",
            "scanner_type": "Flatbed",
        }
        request = rest_framework.test.APIClient()
        request.credentials(HTTP_AUTHORIZATION="Api-Key {}".format(self.key))
        request = request.put(report_url, body)
        self.assertEqual(request.status_code, 200)

        # make sure we can't upload files once the report is sealed
        file_content = io.BytesIO(b"abcdef")
        body = {
            "report": report_url,
            "file_name": "toto.txt",
            "attachment": file_content,
            "secret_key": report_secret,
            "os": "Linux",
            "scan_library": "Libinsane -1.2.3",
            "scan_program": "IronScanner -1.2.3",
            "scan_source": "Flatbed",
            "scanner_type": "Flatbed",
        }
        request = rest_framework.test.APIClient()
        request.credentials(HTTP_AUTHORIZATION="Api-Key {}".format(self.key))
        request = request.post(
            "/api/v1/scannerdb/scan_report_attachments/", body
        )
        self.assertEqual(request.status_code, 404)
