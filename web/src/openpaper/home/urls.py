# -*- coding: utf-8 -*-
from django.urls import re_path

from .views import (
    about,
    download_linux,
    index,
    opensource,
    projects,
    redirect_to_index
)


urlpatterns = [
    re_path(r'^$', index, name="index"),
    re_path(r'^about', about, name='about'),
    re_path(r'^download$', redirect_to_index, name='download'),
    re_path(r'^download/$', redirect_to_index, name='download'),
    re_path(r'^download/linux$', download_linux, name='download_linux'),
    re_path(r'^opensource', opensource, name='opensource'),
    re_path(r'^projects', projects, name='projects'),
]
