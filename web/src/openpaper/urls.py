"""openpaper URL Configuration
"""
from django.conf.urls import include
from django.conf.urls.i18n import i18n_patterns
from django.contrib import admin
from django.contrib import sitemaps
from django.contrib.sitemaps.views import sitemap
from django.urls import (
    re_path,
    reverse
)


class RootViewSitemap(sitemaps.Sitemap):
    priority = 1.0
    changefreq = 'monthly'

    def items(self):
        return [
            'home:index',
        ]

    def location(self, item):
        return reverse(item)


class HighPriorityViewSitemap(sitemaps.Sitemap):
    priority = 0.75
    changefreq = 'monthly'

    def items(self):
        return [
        ]

    def location(self, item):
        return reverse(item)


class AveragePriorityViewSitemap(sitemaps.Sitemap):
    priority = 0.5
    changefreq = 'monthly'

    def items(self):
        return [
            'beacon:statistics',
            'home:opensource',
        ]

    def location(self, item):
        return reverse(item)


class LowPriorityViewSitemap(sitemaps.Sitemap):
    priority = 0.25
    changefreq = 'monthly'

    def items(self):
        return [
            'home:about',
        ]

    def location(self, item):
        return reverse(item)


sitemaps = {
    'root': RootViewSitemap,
    'high_prio': HighPriorityViewSitemap,
    'average_prio': AveragePriorityViewSitemap,
    'low_prio': LowPriorityViewSitemap,
}

urlpatterns = [
    re_path(r'^admin/', admin.site.urls),
    re_path(r'^api/', include('openpaper.api.urls')),
    re_path(r'^beacon/', include('openpaper.beacon.urls')),  # alias for Paperwork
    re_path(
        r'^dashboard/',
        include(('openpaper.dashboard.urls', 'dashboard'), namespace='dashboard')
    ),
    re_path(r'^i18n/', include('django.conf.urls.i18n')),
    # re_path(r'^robots\.txt', include('robots.urls')),
    re_path(  # alias for IronScanner
        r'^scannerdb/',
        include(
            ('openpaper.scannerdb.urls', 'scannerdb'),
            namespace="scannerdb_no_i18n"
        )
    ),
    re_path(
        r'^sitemap\.xml$', sitemap, {'sitemaps': sitemaps},
        name='django.contrib.sitemaps.views.sitemap'
    ),
]

urlpatterns += i18n_patterns(
    re_path(r'^', include(('openpaper.home.urls', 'home'), namespace='home')),
    re_path(
        r'^paperwork/',
        include(('openpaper.beacon.urls', 'beacon'), namespace='beacon')
    ),
    re_path(
        r'^scanner_db/',
        include(('openpaper.scannerdb.urls', 'scannerdb'), namespace='scannerdb')
    ),
)
