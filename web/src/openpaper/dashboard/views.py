from django.contrib.admin.views.decorators import staff_member_required
from django.shortcuts import render

from openpaper.beacon.models import (
    BugCluster,
    BugReport,
    Statistics
)

import openpaper.backend.models.scannerdb


@staff_member_required
def index(request):
    ctx = {
        'unread_bug_reports': BugReport.objects.filter(read=False).count(),
        'unread_clusters': BugCluster.objects.filter(read=False).count(),
        'unmoderated_scan_reports': (
            openpaper.backend.models.scannerdb.ScanReport.objects.filter(
                moderated=False,
                admin_comment=None,
            ).count()
        ),
        'statistics_0': Statistics.objects.count(),
        'statistics_50': Statistics.objects.filter(
            nb_documents__gte=50
        ).count(),
        'statistics_1000': Statistics.objects.filter(
            nb_documents__gte=1000
        ).count(),
    }
    return render(request, 'dashboard.html', ctx)
