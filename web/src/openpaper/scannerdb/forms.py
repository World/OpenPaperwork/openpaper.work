from django import forms


class AdminCommentForm(forms.Form):
    reason = forms.CharField(label='Admin comment', max_length=255)
