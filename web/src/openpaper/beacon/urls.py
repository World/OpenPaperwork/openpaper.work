# -*- coding: utf-8 -*-
from django.urls import path
from django.urls import re_path

from openpaper.beacon.views import bug_cluster_all
from openpaper.beacon.views import bug_cluster_graph
from openpaper.beacon.views import bug_cluster_mark_unread
from openpaper.beacon.views import bug_cluster_recompute
from openpaper.beacon.views import bug_cluster_toggle_solved
from openpaper.beacon.views import bug_cluster_view
from openpaper.beacon.views import bug_report_add_attachment
from openpaper.beacon.views import bug_report_create
from openpaper.beacon.views import bug_report_download_attachment
from openpaper.beacon.views import bug_report_index
from openpaper.beacon.views import bug_report_show
from openpaper.beacon.views import latest
from openpaper.beacon.views import post_statistics
from openpaper.beacon.views import raw_statistics
from openpaper.beacon.views import statistics
from openpaper.beacon.views import statistics_pie
from openpaper.beacon.views import statistics_historic


urlpatterns = [
    path('latest', latest, name='latest'),

    path('post_statistics', post_statistics, name='post_statistics'),
    path('statistics', statistics, name='statistics'),
    path(
        'statistics_historic', statistics_historic,
        name='statistics_historic'
    ),
    re_path(
        r'^statistics_historic'
        r';(?P<split>[a-zA-Z_]*)',
        statistics_historic, name='statistics_historic'
    ),
    path('statistics_pie', statistics_pie, name='statistics_pie'),
    re_path(
        r'^statistics_pie'
        r';(?P<unit>[a-z_]*)'
        r';(?P<min_docs>[0-9]*)'
        r';(?P<split>[a-zA-Z_]*)'
        r';(?P<filters>.*)$',
        statistics_pie, name='statistics_pie'
    ),
    path('raw_statistics', raw_statistics, name='raw_statistics'),

    path('bug_report', bug_report_index, name='bug_report_index'),
    path(
        'bug_report/create', bug_report_create,
        name='bug_report_create'
    ),
    path(
        'bug_report/add_attachment', bug_report_add_attachment,
        name="bug_report_add_attachment"
    ),
    path(
        'bug_report/show/<int:report_id>/<str:secret_key>',
        bug_report_show, name="bug_report_show"
    ),
    path(
        'bug_report/download/'
        '<int:report_id>/'
        '<str:secret_key>/'
        '<int:attachment_id>',
        bug_report_download_attachment, name="bug_report_download_attachment"
    ),

    path(
        'bug_cluster',
        bug_cluster_all,
        name="bug_cluster_all",
    ),
    path(
        'bug_cluster/<int:cluster_id>',
        bug_cluster_view,
        name="bug_cluster_view",
    ),
    path(
        'bug_cluster/<int:cluster_id>/mark_unread',
        bug_cluster_mark_unread,
        name="bug_cluster_mark_unread",
    ),
    path(
        'bug_cluster/<int:cluster_id>/toggle',
        bug_cluster_toggle_solved,
        name="bug_cluster_toggle_solved",
    ),
    path(
        'bug_cluster/<int:cluster_id>/traceback/<int:traceback_id>',
        bug_cluster_view,
        name="bug_cluster_traceback_view",
    ),
    path(
        'bug_cluster/recompute',
        bug_cluster_recompute,
        name="bug_cluster_recompute",
    ),
    path(
        'bug_cluster/graph/<str:graph_name>',
        bug_cluster_graph,
        name="bug_cluster_graph",
    ),
]
