from mailqueue.models import MailerMessage


def send_mail(
            subject, message, from_email, recipient_list, fail_silently=False
        ):
    for recipient in recipient_list:
        new_message = MailerMessage()
        new_message.subject = subject
        new_message.to_address = recipient
        new_message.from_address = from_email
        new_message.content = message
        new_message.app = "OpenPaper.work"
        new_message.save()
