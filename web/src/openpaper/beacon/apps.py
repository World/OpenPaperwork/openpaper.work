from django.apps import AppConfig


class BeaconConfig(AppConfig):
    name = 'openpaper.beacon'
