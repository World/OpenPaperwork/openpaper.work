# Openpaper.work

Django website.


## License

AGPLv3+


## Development

```
docker-compose up
```

Then go to [http://127.0.0.1:1888/](http://127.0.0.1:1888/).


## Translations


### Update translations

[translations.openpaper.work](https://translations.openpaper.work)

Strings to translate are taken from the branch 'master'.


### Update translation files

```
make -C web l10n_extract
make -C web l10n_compile
git add $(find web/l10n -name \*.po)
git commit -m "Update translation files"
git push
```

## Contact

jflesch@openpaper.work
