import logging


LOGGER = logging.getLogger(__name__)


def guess_mime(file_name):
    file_name = file_name.lower()
    if file_name.endswith(".txt") or file_name.endswith(".conf"):
        return "text/plain"
    if file_name.endswith(".png"):
        return "image/png"
    if file_name.endswith(".jpg") or file_name.endswith(".jpeg"):
        return "image/jpeg"
    if file_name.endswith(".pdf"):
        return "application/pdf"
    if file_name.endswith(".json"):
        return "application/json"
    if file_name.endswith(".csv"):
        return "text/csv"
    return "application/octet-stream"
